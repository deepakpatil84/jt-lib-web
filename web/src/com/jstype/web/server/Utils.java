/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.server;

import org.json.*;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class Utils {

	public static JSONArray sharedTypeArrayToJSONArray(JSONSerializable[] values) {
		JSONArray jsa = new JSONArray();
		try {

			for (JSONSerializable value : values) {
				jsa.put(value.toJSON());
			}
		} catch (Exception e) {

		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(boolean[] values) {
		JSONArray jsa = new JSONArray();
		try {

			for (boolean value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {

		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(char[] values) {
		JSONArray jsa = new JSONArray();
		try {

			for (char value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(byte[] values) {
		JSONArray jsa = new JSONArray();
		try {
			for (byte value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(short[] values) {
		JSONArray jsa = new JSONArray();
		try {
			for (short value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(int[] values) {
		JSONArray jsa = new JSONArray();
		try {
			for (int value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(long[] values) {
		JSONArray jsa = new JSONArray();
		try {
			for (long value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(double[] values) {
		JSONArray jsa = new JSONArray();
		try {
			for (double value : values) {
				jsa.put(value);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static JSONArray basicArrayToJSONArray(String[] strings) {
		JSONArray jsa = new JSONArray();
		try {
			for (String str : strings) {
				jsa.put(str);
			}
		} catch (Exception e) {
		}
		return jsa;
	}

	public static String[] JSONArrayToStringArray(String jsondata) {
		String[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new String[len];
			for (i = 0; i < len; i++) {
				ret[i] = URLDecoder.decode(jsa.getString(i), "UTF-8");
			}
		} catch (Exception e) {
			ret = new String[1];
			ret[0] = "Exception";

		}
		return ret;

	}

	public static boolean[] JSONArrayToBooleanArray(String jsondata) {
		boolean[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new boolean[len];
			for (i = 0; i < len; i++) {
				ret[i] = Boolean.parseBoolean(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static char[] JSONArrayToCharArray(String jsondata) {
		char[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new char[len];
			for (i = 0; i < len; i++) {
				ret[i] = jsa.getString(i).charAt(0);
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static byte[] JSONArrayToByteArray(String jsondata) {
		byte[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new byte[len];
			for (i = 0; i < len; i++) {
				ret[i] = Byte.parseByte(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static short[] JSONArrayToShortArray(String jsondata) {
		short[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new short[len];
			for (i = 0; i < len; i++) {
				ret[i] = Short.parseShort(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static int[] JSONArrayToIntArray(String jsondata) {
		int[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new int[len];
			for (i = 0; i < len; i++) {
				ret[i] = Integer.parseInt(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static long[] JSONArrayToLongArray(String jsondata) {
		long[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new long[len];
			for (i = 0; i < len; i++) {
				ret[i] = Long.parseLong(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static float[] JSONArrayToFloatArray(String jsondata) {
		float[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new float[len];
			for (i = 0; i < len; i++) {
				ret[i] = Float.parseFloat(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}

	public static double[] JSONArrayToDoubleArray(String jsondata) {
		double[] ret;
		try {
			JSONArray jsa = new JSONArray(jsondata);
			int len = jsa.length();
			int i;
			ret = new double[len];
			for (i = 0; i < len; i++) {
				ret[i] = Double.parseDouble(jsa.getString(i));
			}
		} catch (Exception e) {
			ret = null;
		}
		return ret;
	}
}

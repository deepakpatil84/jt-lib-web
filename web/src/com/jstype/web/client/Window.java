/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client;

import com.jstype.web.client.dom.FrameElement;
import com.jstype.web.client.dom.Document;



public class Window {
   
    private static Location loc;
    public static native String getDefaultStatus()
    /*-{
        return window.defaultStatus;
    }-*/;

    public static native void setDefaultStatus(String status)
    /*-{
        window.defaultStatus=status;
    }-*/;

    public static native Document getDocument()
    /*-{
        return window.document;
    }-*/;

    public static native FrameElement[] getFrames()
    /*-{
        return window.frames;
    }-*/;

  
    public static native int getInnerHeight()
    /*-{
        return window.innerHeight;
    }-*/;
    public static native void setInnerHeight(int height)
    /*-{
        window.innerHeight=height;
    }-*/;

    public static native int getInnerWidth()/*-{
        return window.innerWidth;
    }-*/;

    public static native void setInnerWidth(int width)/*-{
        window.innerWidth=width;
    }-*/;

    public static native int getLength()/*-{
        return window.length;
    }-*/;

    public static Location getLocation(){
        if(loc==null){
            loc=new Location();
            loc.setObject(getLocationImpl());
        }
        return loc;
    }
    private static native Object getLocationImpl()/*-{
        return window.location;
    }-*/;

    public static native String getName()/*-{
        return window.name;
    }-*/;
    public static native void setName(String name)/*-{
        window.name=name;
    }-*/;
    /**
     *
     * @return
     */
    public static native Navigator getNavigator()/*-{
        return window.navigator;
    }-*/;
    public static native int getOuterHeight()/*-{
        return window.outerHeight;
    }-*/;

    public static native void setOuterHeight(int height)/*-{
        window.outerHeight=height;
    }-*/;
    public static native int getOuterWidth()/*-{
        return window.outerWidth;
    }-*/;
  
    public static native void setOuterWidth(int width)/*-{
        window.outerWidth=width;
    }-*/;
   
    public static native int getPageXOffset()/*-{
        return window.pageXOffset;
    }-*/;
   
    public static native int getPageYOffset()/*-{
        return window.pageYOffset;
    }-*/;
 
    
    public static native int getScreenLeft()/*-{
        return window.screenLeft;
    }-*/;
    
    public static native int getScreenTop()/*-{
        return window.screenTop;
    }-*/;
    
    public static native int getScreenX()/*-{
        return window.screenX;
    }-*/;
    public static native int getSceenY()/*-{
        return window.screenY;
    }-*/;
    
    public static native String getStatus()/*-{
        return window.status;
    }-*/;
    
    public static native void setStatus(String status)/*-{
        window.status=status;
    }-*/;
    
 
    public static native void alert(String message)/*-{
        window.alert(message);
    }-*/;
    //blur not supported by chrome and opera
   
    public static native void clearInterval(int id)/*-{
        window.clearInterval(id);
    }-*/;
 
    public static native void clearTimeout(int id)/*-{
        window.clearTimeout(id);
    }-*/;
   
    public static native void close()/*-{
        window.close();
    }-*/;
  
    public static native boolean confirm(String message)/*-{
        return window.confirm(message);
    }-*/;
  
    public static native void focus()/*-{
        window.focus();
    }-*/;
  
  
    public static native void moveBy(int x,int y)/*-{
        window.moveBy(x,y);
    }-*/;
 
    public static native void moveTo(int x,int y)/*-{
        window.moveTo(x,y);
    }-*/;
   
    public static native void print()/*-{
        window.print();
    }-*/;
    public static native String prompt(String message,String defaultText)/*-{
        return window.prompt(message,defaultText);
    }-*/;
    public static native void scrollBy(int xnum,int ynum)/*-{
        window.scrollBy(xnum,ynum);
    }-*/;
    public static native void scrollTo(int xpos,int ypos)/*-{
        window.scrollTo(xpos,ypos);
    }-*/;
    public static native int setInterval(String code,int millisec)/*-{
        return window.setInterval(code,millisec);
    }-*/;
    public static native int setTimeout(String code,int millisec)/*-{
        return window.setTimeout(code,millisec);
    }-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client;
/**
 * Wraps window.history provide basic functionality to manage and read history 
 * @author Deepak Patil
 *
 */
public class History {
    private Object obj;
    /**
     * Can not be instantiated from outside package.
     * Use Window.getHistory() to get the instance
     */
    History(){}
    /**
     *
     * @param obj
     */
    void setObject(Object obj){
        this.obj=obj;
    }
    /**
     * Returns native window.history object
     * @return
     */
    public Object getObject(){
        return this.obj;
    }
    /**
     *
     * @return
     */
    public native int getLength()/*-{
        return this.obj.length;
    }-*/;
    
    /**
     * Go back to last entry in history
     */
    public native void back()/*-{
        this.obj.back();
    }-*/;
    
    /**
     * Go forward in history
     */
    public native void forward()/*-{
        this.obj.forward();
    }-*/;
    
    /**
     * Go to specific url in history
     * @param url
     */
    public void go(String url){
        this.goURL(url);
    }
    
    /**
     * Goto to specific position in current history
     * @param pos
     */
    public void go(int pos){
        this.goPos(pos);
    }
    
    private native void goPos(int pos)
    /*-{
        this.obj.go(pos);
    }-*/;
    
    private native void goURL(String url)
    /*-{
        this.obj.go(url);
    }-*/;


}

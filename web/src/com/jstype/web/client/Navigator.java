/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client;
public class Navigator {
    private Object obj;
    public Navigator(){
    }
    public void setObject(Object obj){
        this.obj=obj;
    }
    public Object getObject(){
        return this.obj;
    }
    public native String getAppCodeName()/*-{
        return this.obj.appCodeName;
    }-*/;
    public native String getAppName()/*-{
        return this.obj.appName;
    }-*/;
    public native String getAppVersion()/*-{
        return this.obj.appVersion;
    }-*/;
    public native boolean isCookieEnabled()/*-{
        return this.obj.cookieEnabled;
    }-*/;
    public native String getPlatform()/*-{
        return this.obj.platform;
    }-*/;
    public native String getUserAgent()/*-{
        return this.obj.userAgent;
    }-*/;
    public native boolean javaEnabled()/*-{
        return this.obj.javaEnabled();
    }-*/;
    
}

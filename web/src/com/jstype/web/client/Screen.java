/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client;

public class Screen {

    private Object obj;
    Screen(){
    	
    }
    public void setObject(Object obj){
        this.obj=obj;
    }
    public Object getObject(){
        return this.obj;
    }
    public native int getAvailHeight()/*-{
        return this.obj.availHeight;
    }-*/;
    public native int getAvailWidth()/*-{
        return this.obj.availWidth;
    }-*/;
    public native int getColorDepth()/*-{
        return this.obj.colorDepth;
    }-*/;
    public native int getHeight()/*-{
        return this.obj.height;
    }-*/;
    public int getPixelDepth(){
        return this.getColorDepth();
    }
    public native int getWidth()/*-{
        return this.obj.width;
    }-*/;
}

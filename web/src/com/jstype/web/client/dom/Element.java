/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;
import com.jstype.web.client.event.Event;
import com.jstype.web.client.event.EventHandler;
import com.jstype.web.client.event.EventManager;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurEvent;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeEvent;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.ContextMenuEvent;
import com.jstype.web.client.event.dom.ContextMenuHandler;
import com.jstype.web.client.event.dom.CustomEventHandler;
import com.jstype.web.client.event.dom.DoubleClickEvent;
import com.jstype.web.client.event.dom.DoubleClickHandler;
import com.jstype.web.client.event.dom.FocusEvent;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasAllKeyHandlers;
import com.jstype.web.client.event.dom.HasAllMouseHandlers;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasClickHandlers;
import com.jstype.web.client.event.dom.HasContextMenuHandlers;
import com.jstype.web.client.event.dom.HasDoubleClickHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;
import com.jstype.web.client.event.dom.KeyDownEvent;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.event.dom.KeyPressEvent;
import com.jstype.web.client.event.dom.KeyPressHandler;
import com.jstype.web.client.event.dom.KeyUpEvent;
import com.jstype.web.client.event.dom.KeyUpHandler;
import com.jstype.web.client.event.dom.LoadEvent;
import com.jstype.web.client.event.dom.LoadHandler;
import com.jstype.web.client.event.dom.MouseDownEvent;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveEvent;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseOutEvent;
import com.jstype.web.client.event.dom.MouseOutHandler;
import com.jstype.web.client.event.dom.MouseOverEvent;
import com.jstype.web.client.event.dom.MouseOverHandler;
import com.jstype.web.client.event.dom.MouseUpEvent;
import com.jstype.web.client.event.dom.MouseUpHandler;
public abstract class Element extends Node implements
        HasAllKeyHandlers,
        HasAllMouseHandlers,
        HasBlurHandlers,
        HasChangeHandlers,
        HasClickHandlers,
        HasContextMenuHandlers,
        HasDoubleClickHandlers,
        HasFocusHandlers {

    /**
     *
     */
    protected Element() {
    }
    
    public static Element as(Object elm) throws ClassCastException{
    	if(!(elm instanceof Element)){
    		throw new ClassCastException("Can not covert to Element");
    	}
    	return (Element)elm;
	}
    
    public static native boolean is(Object elm)
    /*-{
    	return elm instanceof HTMLElement;
    }-*/;
    
    /**
     * Get native style property of DOM Element
     * @return Style
     */
    public final native Style getStyle() 
    /*-{
    return this.style;
    }-*/;

    /**
     * Fires native blur event on element
     */
    @NoJavaScript
    public final native void blur();

    /**
     * Fires native focus event on element
     */
    @NoJavaScript
    public final native void focus();

    /**
     * Get className string for the dom element
     * if <code><div class='fullWidth top'><\/div></code> it will return '<code>fullWidth top</code>'
     * @return String
     */
    public final native String getCssClassName() 
    /*-{
    	return this.className;
    }-*/;

    /**
     * Gets clientHeight of the DOM element
     * @return int
     */
    public final native int getClientHeight() 
    /*-{
    	return this.clientHeight;
    }-*/;

    /**
     * Gets clientWidth of the DOM element
     * @return
     */
    public final native int getClientWidth() 
    /*-{
    return this.clientWidth;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getDir() 
    /*-{
    return this.dir;
    }-*/;

    @NoJavaScript
    public final native Node[] getElementsByTagName(String tagname);

    /**
     *
     * @return
     */
    public final native String getId() 
    /*-{
    return this.id;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getInnerHTML() 
    /*-{
    return this.innerHTML;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getInnerText() 
    /*-{
    return this.innerText || this.textContent;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getTagName() 
    /*-{
    return this.tagName;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getLang() 
    /*-{
    return this.lang;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getOffsetHeight() 
    /*-{
    return this.offsetHeight || 0;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getOffsetLeft() 
    /*-{
    return this.offsetLeft || 0;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getOffsetTop() /*- {
    return this.offsetTop || 0;
    } -*/;

    /**
     *
     * @return
     */
    public final native int getOffsetWidth() 
    /*-{
    return this.offsetWidth || 0;
    }-*/;

    /**
     *
     * @return
     */
    public final native Element getOffsetParent() 
    /*-{
    return this.offsetParent;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getScrollTop() 
    /*-{
    return this.scrollTop || 0;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getScrollLeft() 
    /*-{
    return this.scrollLeft || 0;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getTitle() 
    /*-{
    return this.title;
    }-*/;

    /**
     *
     * @param title
     */
    public final native void setTitle(String title) 
    /*-{
    this.title=title || '';
    }-*/;

    /**
     *
     * @param attrName
     * @param attrValue
     */
    @NoJavaScript
    public final native void setAttribute(String attrName, String attrValue);

    /**
     *
     * @param name
     * @return
     */
    @NoJavaScript
    public final native int removeAttribute(String name);

    /**
     *
     * @param className
     */
    public final native void setCssClassName(String className) 
    /*-{
    this.className=className;
    }-*/;

    public final void addCssClassName(String className) {
	if (className != null) {
	    String[] classes = className.split(" ");
	    String ex_classname = getCssClassName();
	    int l = classes.length;
	    String cls;
	    if (ex_classname.length() == 0 && classes.length == 1) {
		setCssClassName(className);
	    } else {
		ex_classname = " " + ex_classname + " ";
		for (int i = 0; i < l; i++) {
		    cls = classes[i];
		    if (cls.length() > 0) {
			if (ex_classname.indexOf(" " + cls + " ") < 0) {
			    ex_classname += cls + " ";
			}
		    }
		}
		setCssClassName(ex_classname.trim());
	    }
	}

    }

    public final void removeCssClassName(String className) {
	if (className != null) {
	    String ex_classname = getCssClassName();
	    if (ex_classname.length() > 2) {
		String[] ex_classes = ex_classname.split(" ");
		int l = ex_classes.length;
		String cls;
		className = " " + className + " ";
		ex_classname = "";
		for (int i = 0; i < l; i++) {
		    cls = ex_classes[i];
		    if (cls.length() > 0) {
			if (className.indexOf(" " + cls + " ") < 0) {
			    ex_classname += cls + " ";
			}
		    }
		}
		setCssClassName(ex_classname.trim());
	    }
	}
    }

    public final String[] getCssClassNames() {
	String ex_classname = getCssClassName();
	String[] classes = ex_classname.split(" ");
	int l = classes.length;
	ex_classname = "";
	for (int i = 0; i < l; i++) {
	    String cls = classes[i];
	    if (cls.length() > 0) {
		if (ex_classname.indexOf(" " + cls + " ") < 0) {
		    ex_classname += cls + " ";
		}
	    }
	}
	return ex_classname.trim().split(" ");

    }
    

    /**
     *
     * @param dir
     */
    public final native void setDir(String dir) 
    /*-{
    this.dir=dir;
    }-*/;

    /**
     *
     * @param id
     */
    public final native void setId(String id)
    /*-{
    this.id=id;
    }-*/;

    /**
     *
     * @param html
     */
    public final native void setInnerHTML(String html) 
    /*-{
    this.innerHTML = html || '';
    }-*/;

    /**
     *
     * @param lang
     */
    public final native void setLang(String lang) 
    /*-{
    this.lang=lang;
    }-*/;

   

    /**
     *
     * @param scrollTop
     */
    public final native void setScrollTop(int scrollTop) 
    /*-{
    this.scrollTop=scrollTop;
    }-*/;

    /**
     *
     * @param tabIndex
     */
    public final native void setTabIndex(int tabIndex) 
    /*-{
    this.tabIndex=tabIndex;
    }-*/;
    public final native int getTabIndex()
    /*-{
        return this.tabIndex;
    }-*/;
    private EventManager __eventmanager;

    private void handleNativeEvent(NativeEvent nativeEvent,String eventName){
       Event e;
       if(eventName == null){
	   e=createNativeEvent(nativeEvent,null);
       }else{
	   //TODO:right now only load event is not available on IE
	   e=createNativeEvent(null,eventName);
       }
       if(e!=null){
    	   //this is known event
	   if(nativeEvent != null ){
    	   	__eventmanager.fireEvent(nativeEvent.getType(),e);
	   }else{
	       __eventmanager.fireEvent(eventName,e);
	   }
       }else{
    	   //possible custom event    	
    	   __eventmanager.fireCustomEvent(nativeEvent);
       }
    }
    private  void  registerEventListener(String name){
    	/*
        if(false){
            //just to mark it as used
            //TODO:remove
            this.handleNativeEvent(null);
        }
        */	
        //if(Client.IE && (!Client.IE_9_OR_ABOVE)){
	if(isAttachEvent(this)){
             __registerEventIE(this, name);
        }else{
            __registerEvent(this, name);
        }
    }
    private static native boolean isAttachEvent(Element src)
    /*-{
	return !!src.attachEvent
    }-*/;
    private  void  unregisterEventListener(String name){
        //if(Client.IE && (!Client.IE_9_OR_ABOVE)){
	if(isAttachEvent(this)){
            __unregisterEventIE(this, name);
        }else{
            __unregisterEvent(this, name);
        }
    }
    private native void __registerEvent(Element src,String name)
    /*-{
    	if(name=="load"){
    	    src.onload=this.__eh;
    	}else{
            src.addEventListener(name,this.__eh,false);
        }
    }-*/;
    private native void __unregisterEvent(Element src,String name)
    /*-{
    	if(name=="load"){
    	    src.onload=null;
    	}else{
            src.removeEventListener(name,this.__eh,false);
        }
    }-*/;
     private native void __registerEventIE(Element src,String name)
     /*-{
        if(name=="load"){    	   
    	    var o=this;
    	    src.onreadystatechange=function(a){
    	    	if(src.readyState == "complete"){
    	       		o.__eh("load");
    	       	}
    	    };
    	    
    	}else{
            src.attachEvent("on"+name,this.__eh);
       	}
    }-*/;
    private native void __unregisterEventIE(Element src,String name)
    /*-{
        if(name=="load"){
        	src.onload=null;
        }else{
        	src.detachEvent("on"+name,this.__eh);
        }
    }-*/;
    //TODO:i have used a trick here in order to support IE6 , if you are planning to access DOM classes in native mode
    // please don't do that
    // will update on this soon
    private native void assignEventManagerRef(EventManager ref)
    /*-{
    	this.__em=ref;
        var s=this;
        this.__eh=function(e){
        	if(typeof e=="string"){
        		s.@com.jstype.web.client.dom.Element::handleNativeEvent(Lcom/jstype/web/client/dom/NativeEvent;Ljava/lang/String;)(null,e);
        	}else{            	
        		s.@com.jstype.web.client.dom.Element::handleNativeEvent(Lcom/jstype/web/client/dom/NativeEvent;Ljava/lang/String;)(e,null);
           	}
       	};
    }-*/;
    protected HandlerRegistration addEventHandler(String eventName, final EventHandler handler) {
        if (this.__eventmanager == null) {
            this.__eventmanager = new EventManager();
            this.assignEventManagerRef(__eventmanager);
        }
        final String type= eventName!=null ? eventName : getEventHandlerType(handler);
        
        registerEventListener(type);
        this.__eventmanager.addEventHandler(type, handler);
        return new HandlerRegistration() {
            public void removeHandler() {
                __eventmanager.removeEventHandler(type, handler);
                if(!__eventmanager.hasEventHandlersForType(type)){
                   unregisterEventListener(type);
                }
            }
        };
    }

    public void removeEventHandler(EventHandler handler) {
	if (__eventmanager != null) {
	    String type = getEventHandlerType(handler);
	    __eventmanager.removeEventHandler(type, handler);
	    if (!__eventmanager.hasEventHandlersForType(type)) {
		unregisterEventListener(type);
	    }
	}
    }

    public boolean hasEventHandlersForType(String type) {
	return __eventmanager == null ? false : __eventmanager
		.hasEventHandlersForType(type);
    }

    public void fireEvent(Event<?> event) {
	if (this.__eventmanager != null) {
	    this.__eventmanager.fireEvent(getEventType(event), event);
	}
    }

    public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addBlurHandler(BlurHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addChangeHandler(ChangeHandler handler) {
        return this.addEventHandler(null, handler);
    }
    
    public HandlerRegistration addCustomEventHandler(String eventName,CustomEventHandler handler){
    	return this.addEventHandler(eventName, handler);
    }

    public HandlerRegistration addClickHandler(ClickHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addContextMenuHandler(ContextMenuHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
        return this.addEventHandler(null, handler);
    }

    public HandlerRegistration addFocusHandler(FocusHandler handler) {
        return this.addEventHandler(null, handler);
    }

    private static Event createNativeEvent(NativeEvent nativeEvent,String name){
	if(nativeEvent!=null){
	    name=nativeEvent.getType();
	}
        if(name.equalsIgnoreCase(BlurEvent.NAME)){
            return new BlurEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(ChangeEvent.NAME)){
            return new ChangeEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(ClickEvent.NAME)){
            return new ClickEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(ContextMenuEvent.NAME)){
            return new ContextMenuEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(DoubleClickEvent.NAME)){
            return new DoubleClickEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(FocusEvent.NAME)){
            return new FocusEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(KeyDownEvent.NAME)){
            return new KeyDownEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(KeyPressEvent.NAME)){
            return new KeyPressEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(KeyUpEvent.NAME)){
            return new KeyUpEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(LoadEvent.NAME)){
            return new LoadEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(MouseDownEvent.NAME)){
            return new MouseDownEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(MouseMoveEvent.NAME)){
            return new MouseMoveEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(MouseOutEvent.NAME)){
            return new MouseOutEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(MouseOverEvent.NAME)){
            return new MouseOverEvent(nativeEvent);
        }
        if(name.equalsIgnoreCase(MouseUpEvent.NAME)){
            return new MouseUpEvent(nativeEvent);
        }
        
        //TODO:implement for other events
        return null;
    }
    public static String getEventHandlerType(EventHandler handler){
        if(handler instanceof BlurHandler){
            return BlurEvent.NAME;
        }
        if(handler instanceof ChangeHandler){
            return ChangeEvent.NAME;
        }
        if(handler instanceof ClickHandler){
            return ClickEvent.NAME;
        }
        if(handler instanceof ContextMenuHandler){
            return ContextMenuEvent.NAME;
        }
        if(handler instanceof DoubleClickHandler){
            return DoubleClickEvent.NAME;
        }
        if(handler instanceof FocusHandler){
            return FocusEvent.NAME;
        }
        if(handler instanceof KeyDownHandler){
            return KeyDownEvent.NAME;
        }
        if(handler instanceof KeyPressHandler){
            return KeyPressEvent.NAME;
        }
        if(handler instanceof KeyUpHandler){
            return KeyUpEvent.NAME;
        }
        if(handler instanceof LoadHandler){
            return LoadEvent.NAME;
        }
        if(handler instanceof MouseDownHandler){
            return MouseDownEvent.NAME;
        }
        if(handler instanceof MouseMoveHandler){
            return MouseMoveEvent.NAME;
        }
        if(handler instanceof MouseOutHandler){
            return MouseOutEvent.NAME;
        }
        if(handler instanceof MouseOverHandler){
            return MouseOverEvent.NAME;
        }
        if(handler instanceof MouseUpHandler){
            return MouseUpEvent.NAME;
        }        
        return null;//throw exception over here
    }
     private static String getEventType(Event event){
        if(event instanceof BlurEvent){
            return BlurEvent.NAME;
        }
        if(event instanceof ChangeEvent){
            return ChangeEvent.NAME;
        }
        if(event instanceof ClickEvent){
            return ClickEvent.NAME;
        }
        if(event instanceof ContextMenuEvent){
            return ContextMenuEvent.NAME;
        }
        if(event instanceof DoubleClickEvent){
            return DoubleClickEvent.NAME;
        }
        if(event instanceof FocusEvent){
            return FocusEvent.NAME;
        }
        if(event instanceof KeyDownEvent){
            return KeyDownEvent.NAME;
        }
        if(event instanceof KeyPressEvent){
            return KeyPressEvent.NAME;
        }
        if(event instanceof KeyUpEvent){
            return KeyUpEvent.NAME;
        }
        if(event instanceof LoadEvent){
            return LoadEvent.NAME;
        }
        if(event instanceof MouseDownEvent){
            return MouseDownEvent.NAME;
        }
        if(event instanceof MouseMoveEvent){
            return MouseMoveEvent.NAME;
        }
        if(event instanceof MouseOutEvent){
            return MouseOutEvent.NAME;
        }
        if(event instanceof MouseOverEvent){
            return MouseOverEvent.NAME;
        }
        if(event instanceof MouseUpEvent){
            return MouseUpEvent.NAME;
        }
        
        return null;//throw exception over here
    }
}

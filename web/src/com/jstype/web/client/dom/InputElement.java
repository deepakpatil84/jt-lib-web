/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class InputElement extends Element {

    public static final String TAG = "input";
    
    public static final String TYPE_BUTTON = "button";
    public static final String TYPE_CHECKBOX = "checkbox";
    public static final String TYPE_FILE = "file";
    public static final String TYPE_HIDDEN = "hidden";
    public static final String TYPE_IMAGE = "image";
    public static final String TYPE_PASSWORD = "password";
    public static final String TYPE_RADIO = "radio";
    public static final String TYPE_RESET = "reset";
    public static final String TYPE_SUBMIT = "submit";
    public static final String TYPE_TEXT = "text";
    
    

    public enum InputType {
        BUTTON {
            public String getName(){
                return TYPE_BUTTON;
            }
        },
        CHECKBOX {
            public String getName(){
                return TYPE_CHECKBOX;
            }
        },
        FILE {
            public String getName(){
                return TYPE_FILE;
            }
        },
        HIDDEN {
            public String getName(){
                return TYPE_HIDDEN;
            }
        },
        IMAGE {
            public String getName(){
                return TYPE_IMAGE;
            }
        },
        PASSWORD {
            public String getName(){
                return TYPE_PASSWORD;
            }
        },
        RADIO {
            public String getName(){
                return TYPE_RADIO;
            }
        },
        RESET {
            public String getName(){
                return TYPE_RESET;
            }
        },
        SUBMIT {
            public String getName(){
                return TYPE_SUBMIT;
            }
        },
        TEXT {
            public String getName(){
                return TYPE_TEXT;
            }
        };
        public abstract String getName();
    }
    /**
     *
     */
    protected InputElement() {
    }

    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static InputElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(InputElement.TAG)){
    		throw new ClassCastException("Can not covert to InputElement");
    	}
    	return (InputElement)elm;
    }
    /**
     *
     */
    @NoJavaScript
    public final native void click();

    /**
     *
     * @return
     */
    public final native String getAccept() /*-{
    return this.accept;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getAlt() /*-{
    return this.alt;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getDefaultValue() /*-{
    return this.defaultValue;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getMaxLength() /*-{
    return this.maxLength;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getSize() /*-{
    return this.size;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getSrc() /*-{
    return this.src;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;
    public final native void setType(String type) /*-{
    this.type=type;
    }-*/;
    public final void setType(InputType type){
        this.setType(type.getName());
    }


    /**
     *
     * @return
     */
    public final native String getValue() /*-{
    return this.value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isChecked() /*-{
    return !!this.checked;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDefaultChecked() /*-{
    return !!this.defaultChecked;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isReadOnly() /*-{
    return !!this.readOnly;
    }-*/;

    /**
     *
     */
    @NoJavaScript
    public final native void select(); 

    /**
     *
     * @param accept
     */
    public final native void setAccept(String accept) /*-{
    this.accept = accept;
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param alt
     */
    public final native void setAlt(String alt) /*-{
    this.alt = alt;
    }-*/;

    /**
     *
     * @param checked
     */
    public final native void setChecked(boolean checked) /*-{
    this.checked = checked;
    }-*/;

    /**
     *
     * @param defaultChecked
     */
    public final native void setDefaultChecked(boolean defaultChecked) /*-{
    this.defaultChecked = defaultChecked;
    }-*/;

    /**
     *
     * @param defaultValue
     */
    public final native void setDefaultValue(String defaultValue) /*-{
    this.defaultValue = defaultValue;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param maxLength
     */
    public final native void setMaxLength(int maxLength) /*-{
    this.maxLength = maxLength;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param readOnly
     */
    public final native void setReadOnly(boolean readOnly) /*-{
    this.readOnly = readOnly;
    }-*/;

    /**
     *
     * @param size
     */
    public final native void setSize(int size) /*-{
    this.size = size;
    }-*/;

    /**
     *
     * @param src
     */
    public final native void setSrc(String src) /*-{
    this.src = src;
    }-*/;

    /**
     *
     * @param useMap
     */
    public final native void setUseMap(boolean useMap) /*-{
    this.useMap = useMap;
    }-*/;

    /**
     *
     * @param value
     */
    public final native void setValue(String value) /*-{
    this.value = value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getUseMap() /*-{
    return !!this.useMap;
    }-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class AnchorElement extends Element {

    /**
     *
     */
    public static final String TAG = "a";

    /**
     *
     */
    protected AnchorElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    
    public static AnchorElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(AnchorElement.TAG)){
    		throw new ClassCastException("Can not covert to AnchorElement");
    	}
    	return (AnchorElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHref() /*-{
    return this.href;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHreflang() /*-{
    return this.hreflang;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getRel() /*-{
    return this.rel;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getTarget() /*-{
    return this.target;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param href
     */
    public final native void setHref(String href) /*-{
    this.href = href;
    }-*/;

    /**
     *
     * @param hreflang
     */
    public final native void setHreflang(String hreflang) /*-{
    this.hreflang = hreflang;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param rel
     */
    public final native void setRel(String rel) /*-{
    this.rel = rel;
    }-*/;

    /**
     *
     * @param target
     */
    public final native void setTarget(String target) /*-{
    this.target = target;
    }-*/;

    /**
     *
     * @param type
     */
    public final native void setType(String type) /*-{
    this.type = type;
    }-*/;

	
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class TableCellElement extends Element {

    public static final String TAG_TD = "td";
    public static final String TAG_TH = "th";

    /**
     *
     */
    protected TableCellElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? (((Element) elm).getTagName().equalsIgnoreCase(
		        TableCellElement.TAG_TD) || ((Element) elm).getTagName()
		        .equalsIgnoreCase(TableCellElement.TAG_TH)) : false;
	}
	public static TableCellElement as(Object elm) {
		
		if (!(((Element)elm).getTagName().equalsIgnoreCase(TableCellElement.TAG_TD) || ((Element)elm)
		        .getTagName().equalsIgnoreCase(TableCellElement.TAG_TH))) {
			throw new ClassCastException("Can not covert to TableCellElement");
		}
		return (TableCellElement) elm;
	}

    /**
     *
     * @return
     */
    public final native String getAlign() /*-{
    return this.align;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getCellIndex() /*-{
    return this.cellIndex;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCh() /*-{
    return this.ch;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getChOff() /*-{
    return this.chOff;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getColSpan() /*-{
    return this.colSpan;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHeaders() /*-{
    return this.headers;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getRowSpan() /*-{
    return this.rowSpan;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getVAlign() /*-{
    return this.vAlign;
    }-*/;

    /**
     *
     * @param align
     */
    public final native void setAlign(String align) /*-{
    this.align = align;
    }-*/;

    /**
     *
     * @param ch
     */
    public final native void setCh(String ch) /*-{
    this.ch = ch;
    }-*/;

    /**
     *
     * @param chOff
     */
    public final native void setChOff(String chOff) /*-{
    this.chOff = chOff;
    }-*/;

    /**
     *
     * @param colSpan
     */
    public final native void setColSpan(int colSpan) /*-{
    this.colSpan = colSpan;
    }-*/;

    /**
     *
     * @param headers
     */
    public final native void setHeaders(String headers) /*-{
    this.headers = headers;
    }-*/;

    /**
     *
     * @param rowSpan
     */
    public final native void setRowSpan(int rowSpan) /*-{
    this.rowSpan = rowSpan;
    }-*/;

    /**
     *
     * @param vAlign
     */
    public final native void setVAlign(String vAlign) /*-{
    this.vAlign = vAlign;
    }-*/;
}

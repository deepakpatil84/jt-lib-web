/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class ImageElement extends Element {

    public static final String TAG = "img";

    /**
     *
     */
    protected ImageElement() {
    }

    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static ImageElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(ImageElement.TAG)){
    		throw new ClassCastException("Can not covert to ImageElement");
    	}
    	return (ImageElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getAlt() /*-{
    return this.alt;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getHeight() /*-{
    return this.height;
    }-*/;
    //TODO:check this

    /**
     *
     * @return
     */
    public final native String getSrc() /*- {
    return this.src;
    //return DOMImpl.impl.imgGetSrc(this);
    }-*/;

    /**
     *
     * @return
     */
    public final native int getWidth() /*-{
    return this.width;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getIsMap() /*-{
    return !!this.isMap;
    }-*/;

    /**
     *
     * @param alt
     */
    public final native void setAlt(String alt) /*-{
    this.alt = alt;
    }-*/;

    /**
     *
     * @param height
     */
    public final native void setHeight(int height) /*-{
    this.height = height + "px";
    }-*/;

    /**
     *
     * @param isMap
     */
    public final native void setIsMap(boolean isMap) /*-{
    this.isMap = isMap;
    }-*/;
    //TODO:check this

    /**
     *
     * @param src
     */
    public final native void setSrc(String src)/*- {
    this.src=src;
    //DOMImpl.impl.imgSetSrc(this, src);
    }-*/;

    /**
     *
     * @param useMap
     */
    public final native void setUseMap(boolean useMap) /*-{
    this.useMap = useMap;
    }-*/;

    /**
     *
     * @param width
     */
    public final native void setWidth(int width) /*-{
    this.width = width + "px";
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getUseMap() /*-{
    return !!this.useMap;
    }-*/;
}

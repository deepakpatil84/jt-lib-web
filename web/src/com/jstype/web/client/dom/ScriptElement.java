/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.web.client.event.dom.LoadHandler;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.HasLoadHandlers;

@NativeNonInstanciable
public class ScriptElement extends Element implements HasLoadHandlers{

    public static final String TAG = "script";

    /**
     *
     */
    protected ScriptElement() {
    }

	public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static ScriptElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(ScriptElement.TAG)){
    		throw new ClassCastException("Can not covert to ScriptElement");
    	}
    	return (ScriptElement)elm;
    }
    /**
     *
     * @return
     */
    public final native String getDefer() /*-{
    return this.defer;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getSrc() /*-{
    return this.src;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getText() /*-{
    return this.text;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @param defer
     */
    public final native void setDefer(String defer) /*-{
    this.defer = defer;
    }-*/;

    /**
     *
     * @param src
     */
    public final native void setSrc(String src) /*-{
    this.src = src;
    }-*/;

    /**
     *
     * @param text
     */
    public final native void setText(String text) /*-{
    this.text = text;
    }-*/;

    /**
     *
     * @param type
     */
    public final native void setType(String type) /*-{
    this.type = type;
    }-*/;
    
    public HandlerRegistration addLoadHandler(LoadHandler handler){
        return this.addEventHandler(null, handler);
    }
}

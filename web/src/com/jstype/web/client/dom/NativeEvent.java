/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;
import com.jstype.web.client.Client;

@NativeNonInstanciable
public class NativeEvent  {

    public static final int BUTTON_LEFT = 0;
    public static final int BUTTON_RIGHT = 2;
    public static final int BUTTON_MIDDLE = 1;

    protected NativeEvent() {
    }
    //TODO:define this

    public final native String getType()/*- {
         return this.type;
    } -*/;
    //TODO:define this

    public final native boolean getAltKey()/*- {
         return this.altKey;
    } -*/;

    //TODO:define this
    public final native boolean getCtrlKey()/*- {
        return this.ctrlKey;
     } -*/;

    //TODO:define this
    public final native boolean getShiftKey()/*- {
        return this.shiftKey;
     } -*/;

    //TODO:define this
    public final  int getButton(){
    	int value=_getButton();
    	if(Client.IE && !Client.IE_9_OR_ABOVE && value != 2){
    		value = value == 1 ? 0 : value == 4 ? 1 : 2;  
    	}
    	return value;
    }
    private final native int _getButton()/*- {
        return this.button;
     } -*/;

    //TODO:define this
    public final native int getCharCode()/*- {
        return -1;
     } -*/;

    //TODO:define this
    public final native int getClientX()/*- {
        return this.pageX || this.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
     } -*/;

    //TODO:define this
    public final native int getClientY()/*- {
        return this.pageY || this.clientY + document.body.scrollTop + document.documentElement.scrollTop;
     } -*/;

    //TODO:define this
    public final native int getKeyCode()/*- {
        return this.keyCode;
     } -*/;


    //TODO:define this
    public final native boolean getMetaKey()/*- {
        return this.metaKey;
    } -*/;

    //TODO:define this
    public final native int getScreenX()/*- {
        return parseInt(this.screenX);
    } -*/;

    //TODO:define this
    public final native int getScreenY()/*- {
        return parseInt(this.screenY);
    } -*/;

    //TODO:check this
    public final native Element getTarget()/*- {
      //if(this.target)
      //   return this.target;
      //if(this.srcElement)
      //   return this.srcElement;
      //return null;
      return this.target || this.srcElement || null;
    }-*/;
    @NoJavaScript //placeholder to protect native javascript method
    private native void preventDefault(int a);
    @NoJavaScript //placeholder to protect native javascript method
    private native void stopPropagation(int a);
    
    //TODO:define this
    public final void preventDefault(){
        //if(Client.IE && !(Client.IE_9_OR_ABOVE)){
        this.preventDefault_IE_Legacy();
        //}else{
        if(this.hasPreventDefaultMethod()){
            this.preventDefault(0);
        }
        //}
    }
    private native void preventDefault_IE_Legacy()/*-{
	if(this.hasOwnProperty('returnValue'))
	    this.returnValue=false;	
    }-*/;
    private native boolean hasPreventDefaultMethod()/*-{
	return !!this.preventDefault;		
    }-*/;
    //TODO:define this
    public final void stopPropagation(){
        //if(Client.IE && !(Client.IE_9_OR_ABOVE)){
        this.stopPropagation_IE_Legacy();
        //}else{
        if(this.hasStopPropagationMethod()){
            this.stopPropagation(0);
        }
        //}
     }
    private native void stopPropagation_IE_Legacy()/*-{
    	 if(this.hasOwnProperty("cancelBubble"))
             this.cancelBubble=true;
    }-*/;
    
    private native boolean hasStopPropagationMethod()/*-{
    	return !!this.stopPropagation;
    }-*/;

}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class FormElement extends Element {

    public static final String TAG = "form";

    /**
     *
     */
    protected FormElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static FormElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(FormElement.TAG)){
    		throw new ClassCastException("Can not covert to FormElement");
    	}
    	return (FormElement)elm;
    }
    /**
     *
     * @return
     */
    public final native String getAcceptCharset() /*-{
    return this.acceptCharset;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getAction() /*-{
    return this.action;
    }-*/;
    //TODO:Verify this this was  NodeCollection

    /**
     *
     * @return
     */
    public final native Element[] getElements() /*-{
    return this.elements;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getEnctype() /*-{
    return this.enctype;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getMethod() /*-{
    return this.method;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getTarget() /*-{
    return this.target;
    }-*/;

    /**
     *
     */
    public final native void reset() /*-{
    this.reset();
    }-*/;

    /**
     *
     * @param acceptCharset
     */
    public final native void setAcceptCharset(String acceptCharset) /*-{
    this.acceptCharset = acceptCharset;
    }-*/;

    /**
     *
     * @param action
     */
    public final native void setAction(String action) /*-{
    this.action = action;
    }-*/;

    /**
     *
     * @param enctype
     */
    public final native void setEnctype(String enctype) /*-{
    this.enctype = enctype;
    }-*/;

    /**
     *
     * @param method
     */
    public final native void setMethod(String method) /*-{
    this.method = method;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param target
     */
    public final native void setTarget(String target) /*-{
    this.target = target;
    }-*/;

    /**
     *
     */
    public final native void submit() /*-{
    this.submit();
    }-*/;
}

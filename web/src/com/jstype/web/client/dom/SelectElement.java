/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class SelectElement extends Element {

    public static final String TAG = "select";

    /**
     *
     */
    protected SelectElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(
		        SelectElement.TAG) : false;
	}
    
    public static SelectElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(SelectElement.TAG)){
    		throw new ClassCastException("Can not covert to SelectElement");
    	}
    	return (SelectElement)elm;
    }

    //TODO:update this
    /**
     *
     * @param option
     * @param before
     */
    public final void add(OptionElement option, OptionElement before) {
        //DOMImpl.impl.selectAdd(this, option, before);
    }

    //TODO:Update this
    /**
     *
     */
    public final void clear() {
        //DOMImpl.impl.selectClear(this);
    }

    /**
     *
     * @return
     */
    public final native String getDisabled() /*-{
    return this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;
    //TODO:Update this

    /**
     *
     * @return
     */
    public final int getLength() {
        //return DOMImpl.impl.selectGetLength(this);
        return 0;//TODO:
    }

    /**
     *
     * @return
     */
    public final native String getMultiple() /*-{
    return this.multiple;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    //TODO:Update this
    /**
     *
     * @return
     */
    public final native OptionElement[] getOptions() /*-{
    //return DOMImpl.impl.selectGetOptions(this);
    }-*/;

    /**
     *
     * @return
     */
    public final native int getSelectedIndex() /*-{
    return this.selectedIndex;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getSize() /*-{
    return this.size;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getValue() /*-{
    return this.value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isMultiple() /*-{
    return !!this.multiple;
    }-*/;
    //TODO:Update this

    /**
     *
     * @param index
     */
    public final void remove(int index) {
        //DOMImpl.impl.selectRemoveOption(this, index);
    }

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(String disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param multiple
     */
    public final native void setMultiple(boolean multiple) /*-{
    this.multiple = multiple;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param index
     */
    public final native void setSelectedIndex(int index) /*-{
    this.selectedIndex = index;
    }-*/;

    /**
     *
     * @param size
     */
    public final native void setSize(int size) /*-{
    this.size = size;
    }-*/;

    /**
     *
     * @param type
     */
    public final native void setType(String type) /*-{
    this.type = type;
    }-*/;

    /**
     *
     * @param value
     */
    public final native void setValue(String value) /*-{
    this.value = value;
    }-*/;
}

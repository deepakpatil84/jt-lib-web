/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class LabelElement extends Element {

    public static final String TAG = "label";

    /**
     *
     */
    protected LabelElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static LabelElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(LabelElement.TAG)){
    		throw new ClassCastException("Can not covert to LabelElement");
    	}
    	return (LabelElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHtmlFor() /*-{
    return this.htmlFor;
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param htmlFor
     */
    public final native void setHtmlFor(String htmlFor) /*-{
    this.htmlFor = htmlFor;
    }-*/;
}

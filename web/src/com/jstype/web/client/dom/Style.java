/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * Original source from Google Web Toolkit(GWT) 
 * http://code.google.com/webtoolkit/
 *
 * Modified to adapt JsType Framework
 */

/* Original GWT License */

/*
 * Copyright 2008 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.web.client.Client;

/**
 *
 * @link:http://www.w3.org/2003/01/dom2-javadoc/org/w3c/dom/css/CSSStyleDeclaration.html
 */

@NativeNonInstanciable
public class Style {

    private static final String STYLE_Z_INDEX = "zIndex";
    private static final String STYLE_WIDTH = "width";
    private static final String STYLE_VISIBILITY = "visibility";
    private static final String STYLE_TOP = "top";
    private static final String STYLE_TEXT_DECORATION = "textDecoration";
    private static final String STYLE_TEXT_ALIGN = "textAlign";
    private static final String STYLE_RIGHT = "right";
    private static final String STYLE_POSITION = "position";
    private static final String STYLE_PADDING_TOP = "paddingTop";
    private static final String STYLE_PADDING_RIGHT = "paddingRight";
    private static final String STYLE_PADDING_LEFT = "paddingLeft";
    private static final String STYLE_PADDING_BOTTOM = "paddingBottom";
    private static final String STYLE_PADDING = "padding";
    private static final String STYLE_OVERFLOW = "overflow";
    private static final String STYLE_OVERFLOW_X = "overflowX";
    private static final String STYLE_OVERFLOW_Y = "overflowY";
    private static final String STYLE_OPACITY = "opacity";
    private static final String STYLE_MARGIN_TOP = "marginTop";
    private static final String STYLE_MARGIN_RIGHT = "marginRight";
    private static final String STYLE_MARGIN_LEFT = "marginLeft";
    private static final String STYLE_MARGIN_BOTTOM = "marginBottom";
    private static final String STYLE_MARGIN = "margin";
    private static final String STYLE_LIST_STYLE_TYPE = "listStyleType";
    private static final String STYLE_LEFT = "left";
    private static final String STYLE_HEIGHT = "height";
    private static final String STYLE_FONT_WEIGHT = "fontWeight";
    private static final String STYLE_FONT_STYLE = "fontStyle";
    private static final String STYLE_FONT_SIZE = "fontSize";
    private static final String STYLE_DISPLAY = "display";
    private static final String STYLE_FLOAT = "cssFloat";
    private static final String STYLE_FLOAT_TRIDENT = "styleFloat";
    private static final String STYLE_CURSOR = "cursor";
    private static final String STYLE_COLOR = "color";
    private static final String STYLE_BOTTOM = "bottom";
    private static final String STYLE_BORDER_WIDTH = "borderWidth";
    private static final String STYLE_BORDER_STYLE = "borderStyle";
    private static final String STYLE_BORDER_COLOR = "borderColor";
    private static final String STYLE_BACKGROUND_IMAGE = "backgroundImage";
    private static final String STYLE_BACKGROUND_COLOR = "backgroundColor";
    private static final String STYLE_VERTICAL_ALIGN = "verticalAlign";
    private static final String STYLE_TABLE_LAYOUT = "tableLayout";
    private static final String STYLE_OUTLINE_WIDTH = "outlineWidth";
    private static final String STYLE_OUTLINE_STYLE = "outlineStyle";
    private static final String STYLE_OUTLINE_COLOR = "outlineColor";
    private static final String STYLE_LINE_HEIGHT = "lineHeight";

   
  private static final String BORDER_STYLE_SOLID = "solid";
  private static final String BORDER_STYLE_DASHED = "dashed";
  private static final String BORDER_STYLE_DOTTED = "dotted";
  private static final String BORDER_STYLE_HIDDEN = "hidden";
  private static final String BORDER_STYLE_NONE = "none";

  private static final String CURSOR_ROW_RESIZE = "row-resize";
  private static final String CURSOR_COL_RESIZE = "col-resize";
  private static final String CURSOR_HELP = "help";
  private static final String CURSOR_WAIT = "wait";
  private static final String CURSOR_TEXT = "text";
  private static final String CURSOR_W_RESIZE = "w-resize";
  private static final String CURSOR_S_RESIZE = "s-resize";
  private static final String CURSOR_SW_RESIZE = "sw-resize";
  private static final String CURSOR_SE_RESIZE = "se-resize";
  private static final String CURSOR_N_RESIZE = "n-resize";
  private static final String CURSOR_NW_RESIZE = "nw-resize";
  private static final String CURSOR_NE_RESIZE = "ne-resize";
  private static final String CURSOR_E_RESIZE = "e-resize";
  private static final String CURSOR_MOVE = "move";
  private static final String CURSOR_POINTER = "pointer";
  private static final String CURSOR_CROSSHAIR = "crosshair";
  private static final String CURSOR_AUTO = "auto";
  private static final String CURSOR_DEFAULT = "default";

  private static final String DISPLAY_INLINE_BLOCK = "inline-block";
  private static final String DISPLAY_INLINE = "inline";
  private static final String DISPLAY_BLOCK = "block";
  private static final String DISPLAY_NONE = "none";

  private static final String FLOAT_LEFT = "left";
  private static final String FLOAT_RIGHT = "right";
  private static final String FLOAT_NONE = "none";

  private static final String FONT_STYLE_OBLIQUE = "oblique";
  private static final String FONT_STYLE_ITALIC = "italic";
  private static final String FONT_STYLE_NORMAL = "normal";

  private static final String FONT_WEIGHT_LIGHTER = "lighter";
  private static final String FONT_WEIGHT_BOLDER = "bolder";
  private static final String FONT_WEIGHT_BOLD = "bold";
  private static final String FONT_WEIGHT_NORMAL = "normal";

  private static final String LIST_STYLE_TYPE_UPPER_ROMAN = "upper-roman";
  private static final String LIST_STYLE_TYPE_LOWER_ROMAN = "lower-roman";
  private static final String LIST_STYLE_TYPE_UPPER_ALPHA = "upper-alpha";
  private static final String LIST_STYLE_TYPE_LOWER_ALPHA = "lower-alpha";
  private static final String LIST_STYLE_TYPE_DECIMAL = "decimal";
  private static final String LIST_STYLE_TYPE_SQUARE = "square";
  private static final String LIST_STYLE_TYPE_CIRCLE = "circle";
  private static final String LIST_STYLE_TYPE_DISC = "disc";
  private static final String LIST_STYLE_TYPE_NONE = "none";

  private static final String OUTLINE_STYLE_DASHED = "dashed";
  private static final String OUTLINE_STYLE_DOTTED = "dotted";
  private static final String OUTLINE_STYLE_DOUBLE = "double";
  private static final String OUTLINE_STYLE_GROOVE = "groove";
  private static final String OUTLINE_STYLE_INSET = "inset";
  private static final String OUTLINE_STYLE_NONE = "none";
  private static final String OUTLINE_STYLE_OUTSET = "outset";
  private static final String OUTLINE_STYLE_RIDGE = "ridge";
  private static final String OUTLINE_STYLE_SOLID = "solid";

  private static final String OVERFLOW_AUTO = "auto";
  private static final String OVERFLOW_SCROLL = "scroll";
  private static final String OVERFLOW_HIDDEN = "hidden";
  private static final String OVERFLOW_VISIBLE = "visible";

  private static final String POSITION_FIXED = "fixed";
  private static final String POSITION_ABSOLUTE = "absolute";
  private static final String POSITION_RELATIVE = "relative";
  private static final String POSITION_STATIC = "static";

  private static final String UNIT_MM = "mm";
    private static final String UNIT_CM = "cm";
    private static final String UNIT_IN = "in";
    private static final String UNIT_PC = "pc";
    private static final String UNIT_PT = "pt";
    private static final String UNIT_EX = "ex";
    private static final String UNIT_EM = "em";
    private static final String UNIT_PCT = "%";
    private static final String UNIT_PX = "px";

  private static final String TABLE_LAYOUT_AUTO = "auto";
  private static final String TABLE_LAYOUT_FIXED = "fixed";

  private static final String TEXT_DECORATION_LINE_THROUGH = "line-through";
  private static final String TEXT_DECORATION_OVERLINE = "overline";
  private static final String TEXT_DECORATION_UNDERLINE = "underline";
  private static final String TEXT_DECORATION_NONE = "none";
  private static final String VERTICAL_ALIGN_BASELINE = "baseline";
  private static final String VERTICAL_ALIGN_SUB = "sub";
  private static final String VERTICAL_ALIGN_SUPER = "super";
  private static final String VERTICAL_ALIGN_TOP = "top";
  private static final String VERTICAL_ALIGN_TEXT_TOP = "text-top";
  private static final String VERTICAL_ALIGN_MIDDLE = "middle";
  private static final String VERTICAL_ALIGN_BOTTOM = "bottom";
  private static final String VERTICAL_ALIGN_TEXT_BOTTOM = "text-bottom";

  private static final String VISIBILITY_HIDDEN = "hidden";
  private static final String VISIBILITY_VISIBLE = "visible";
  
  protected Style() {
  }

  public interface HasCssName {
      String getCssName();
  }
  
  public enum BorderStyle implements HasCssName {

        NONE {

            @Override
            public String getCssName() {
                return BORDER_STYLE_NONE;
            }
        },
        DOTTED {

            @Override
            public String getCssName() {
                return BORDER_STYLE_DOTTED;
            }
        },
        DASHED {

            @Override
            public String getCssName() {
                return BORDER_STYLE_DASHED;
            }
        },
        HIDDEN {

            @Override
            public String getCssName() {
                return BORDER_STYLE_HIDDEN;
            }
        },
        SOLID {

            @Override
            public String getCssName() {
                return BORDER_STYLE_SOLID;
            }
        };

        public abstract String getCssName();
    }
    
    public enum Unit {

        PX {

            @Override
            public String getType() {
                return UNIT_PX;
            }
        }, PCT {

            @Override
            public String getType() {
                return UNIT_PCT;
            }
        }, EM {

            @Override
            public String getType() {
                return UNIT_EM;
            }
        }, EX {

            @Override
            public String getType() {
                return UNIT_EX;
            }
        }, PT {

            @Override
            public String getType() {
                return UNIT_PT;
            }
        }, PC {

            @Override
            public String getType() {
                return UNIT_PC;
            }
        }, IN {

            @Override
            public String getType() {
                return UNIT_IN;
            }
        }, CM {

            @Override
            public String getType() {
                return UNIT_CM;
            }
        }, MM {

            @Override
            public String getType() {
                return UNIT_MM;
            }
        };

        public abstract String getType();
    }
  public enum Cursor implements HasCssName {
    DEFAULT {
      @Override
      public String getCssName() {
        return CURSOR_DEFAULT;
      }
    },
    AUTO {
      @Override
      public String getCssName() {
        return CURSOR_AUTO;
      }
    },
    CROSSHAIR {
      @Override
      public String getCssName() {
        return CURSOR_CROSSHAIR;
      }
    },
    POINTER {
      @Override
      public String getCssName() {
        return CURSOR_POINTER;
      }
    },
    MOVE {
      @Override
      public String getCssName() {
        return CURSOR_MOVE;
      }
    },
    E_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_E_RESIZE;
      }
    },
    NE_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_NE_RESIZE;
      }
    },
    NW_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_NW_RESIZE;
      }
    },
    N_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_N_RESIZE;
      }
    },
    SE_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_SE_RESIZE;
      }
    },
    SW_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_SW_RESIZE;
      }
    },
    S_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_S_RESIZE;
      }
    },
    W_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_W_RESIZE;
      }
    },
    TEXT {
      @Override
      public String getCssName() {
        return CURSOR_TEXT;
      }
    },
    WAIT {
      @Override
      public String getCssName() {
        return CURSOR_WAIT;
      }
    },
    HELP {
      @Override
      public String getCssName() {
        return CURSOR_HELP;
      }
    },
    COL_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_COL_RESIZE;
      }
    },
    ROW_RESIZE {
      @Override
      public String getCssName() {
        return CURSOR_ROW_RESIZE;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the display property.
   */
  public enum Display implements HasCssName {
    NONE {
      @Override
      public String getCssName() {
        return DISPLAY_NONE;
      }
    },
    BLOCK {
      @Override
      public String getCssName() {
        return DISPLAY_BLOCK;
      }
    },
    INLINE {
      @Override
      public String getCssName() {
        return DISPLAY_INLINE;
      }
    },
    INLINE_BLOCK {
      @Override
      public String getCssName() {
        return DISPLAY_INLINE_BLOCK;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the float property.
   */
  public enum Float implements HasCssName {
    LEFT {
      @Override
      public String getCssName() {
        return FLOAT_LEFT;
      }
    },
    RIGHT {
      @Override
      public String getCssName() {
        return FLOAT_RIGHT;
      }
    },
    NONE {
      @Override
      public String getCssName() {
        return FLOAT_NONE;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the font-style property.
   */
  public enum FontStyle implements HasCssName {
    NORMAL {
      @Override
      public String getCssName() {
        return FONT_STYLE_NORMAL;
      }
    },
    ITALIC {
      @Override
      public String getCssName() {
        return FONT_STYLE_ITALIC;
      }
    },
    OBLIQUE {
      @Override
      public String getCssName() {
        return FONT_STYLE_OBLIQUE;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the font-weight property.
   */
  public enum FontWeight implements HasCssName {
    NORMAL {
      @Override
      public String getCssName() {
        return FONT_WEIGHT_NORMAL;
      }
    },
    BOLD {
      @Override
      public String getCssName() {
        return FONT_WEIGHT_BOLD;
      }
    },
    BOLDER {
      @Override
      public String getCssName() {
        return FONT_WEIGHT_BOLDER;
      }
    },
    LIGHTER {
      @Override
      public String getCssName() {
        return FONT_WEIGHT_LIGHTER;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the list-style-type property.
   */
  public enum ListStyleType implements HasCssName {
    NONE {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_NONE;
      }
    },
    DISC {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_DISC;
      }
    },
    CIRCLE {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_CIRCLE;
      }
    },
    SQUARE {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_SQUARE;
      }
    },
    DECIMAL {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_DECIMAL;
      }
    },
    LOWER_ALPHA {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_LOWER_ALPHA;
      }
    },
    UPPER_ALPHA {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_UPPER_ALPHA;
      }
    },
    LOWER_ROMAN {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_LOWER_ROMAN;
      }
    },
    UPPER_ROMAN {
      @Override
      public String getCssName() {
        return LIST_STYLE_TYPE_UPPER_ROMAN;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the outline-style property.
   */
  public enum OutlineStyle implements HasCssName {
    NONE {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_NONE;
      }
    },
    DASHED {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_DASHED;
      }
    },
    DOTTED {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_DOTTED;
      }
    },
    DOUBLE {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_DOUBLE;
      }
    },
    GROOVE {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_GROOVE;
      }
    },
    INSET {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_INSET;
      }
    },
    OUTSET {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_OUTSET;
      }
    },
    RIDGE {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_RIDGE;
      }
    },
    SOLID {
      @Override
      public String getCssName() {
        return OUTLINE_STYLE_SOLID;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the overflow property.
   */
  public enum Overflow implements HasCssName {
    VISIBLE {
      @Override
      public String getCssName() {
        return OVERFLOW_VISIBLE;
      }
    },
    HIDDEN {
      @Override
      public String getCssName() {
        return OVERFLOW_HIDDEN;
      }
    },
    SCROLL {
      @Override
      public String getCssName() {
        return OVERFLOW_SCROLL;
      }
    },
    AUTO {
      @Override
      public String getCssName() {
        return OVERFLOW_AUTO;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the display property.
   */
  public enum Position implements HasCssName {
    STATIC {
      @Override
      public String getCssName() {
        return POSITION_STATIC;
      }
    },
    RELATIVE {
      @Override
      public String getCssName() {
        return POSITION_RELATIVE;
      }
    },
    ABSOLUTE {
      @Override
      public String getCssName() {
        return POSITION_ABSOLUTE;
      }
    },
    FIXED {
      @Override
      public String getCssName() {
        return POSITION_FIXED;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the table-layout property.
   */
  public enum TableLayout implements HasCssName {
    AUTO {
      @Override
      public String getCssName() {
        return TABLE_LAYOUT_AUTO;
      }
    },
    FIXED {
      @Override
      public String getCssName() {
        return TABLE_LAYOUT_FIXED;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the text-decoration property.
   */
  public enum TextDecoration implements HasCssName {
    NONE {
      @Override
      public String getCssName() {
        return TEXT_DECORATION_NONE;
      }
    },
    UNDERLINE {
      @Override
      public String getCssName() {
        return TEXT_DECORATION_UNDERLINE;
      }
    },
    OVERLINE {
      @Override
      public String getCssName() {
        return TEXT_DECORATION_OVERLINE;
      }
    },
    LINE_THROUGH {
      @Override
      public String getCssName() {
        return TEXT_DECORATION_LINE_THROUGH;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the vertical-align property.
   */
  public enum VerticalAlign implements HasCssName {
    BASELINE {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_BASELINE;
      }
    },
    SUB {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_SUB;
      }
    },
    SUPER {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_SUPER;
      }
    },
    TOP {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_TOP;
      }
    },
    TEXT_TOP {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_TEXT_TOP;
      }
    },
    MIDDLE {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_MIDDLE;
      }
    },
    BOTTOM {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_BOTTOM;
      }
    },
    TEXT_BOTTOM {
      @Override
      public String getCssName() {
        return VERTICAL_ALIGN_TEXT_BOTTOM;
      }
    };
    public abstract String getCssName();
  }

  /**
   * Enum for the visibility property.
   */
  public enum Visibility implements HasCssName {
    VISIBLE {
      @Override
      public String getCssName() {
        return VISIBILITY_VISIBLE;
      }
    },
    HIDDEN {
      @Override
      public String getCssName() {
        return VISIBILITY_HIDDEN;
      }
    };
    public abstract String getCssName();
  }

    public final void setBackgroundColor(String value) {
        setPropertyValue(STYLE_BACKGROUND_COLOR, value);
    }

    public final String getBackgroundColor() {
        return getPropertyValue(STYLE_BACKGROUND_COLOR);
    }

    public final void clearBackgroundColor() {
        clearProperty(STYLE_BACKGROUND_COLOR);
    }

    public final void setBackgroundImage(String value) {
        setPropertyValue(STYLE_BACKGROUND_IMAGE, value);
    }

    public final String getBackgroundImage() {
        return getPropertyValue(STYLE_BACKGROUND_IMAGE);
    }

    public final void clearBackgroundImage() {
        clearProperty(STYLE_BACKGROUND_IMAGE);
    }

    public final void setBorderColor(String value) {
        setPropertyValue(STYLE_BORDER_COLOR, value);
    }

    public final String getBorderColor() {
        return getPropertyValue(STYLE_BORDER_COLOR);
    }

    public final void clearBorderColor() {
        clearProperty(STYLE_BORDER_COLOR);
    }

    public final void setBorderStyle(String value) {
        setPropertyValue(STYLE_BORDER_STYLE, value);
    }

    public final String getBorderSyle() {
        return getPropertyValue(STYLE_BORDER_STYLE);
    }

    public final void clearBorderStyle() {
        clearProperty(STYLE_BORDER_STYLE);
    }

    public final void setBorderWidth(String value) {
        setPropertyValue(STYLE_BORDER_WIDTH, value);
    }

    public final String getBorderWidth() {
        return getPropertyValue(STYLE_BORDER_WIDTH);
    }

    public final void clearBorderWidth() {
        clearProperty(STYLE_BORDER_WIDTH);
    }

    public final void setBottom(String value) {
        setPropertyValue(STYLE_BOTTOM, value);
    }

    public final String getBottom() {
        return getPropertyValue(STYLE_BOTTOM);
    }

    public final void clearBottom() {
        clearProperty(STYLE_BOTTOM);
    }

    public final void setColor(String value) {
        setPropertyValue(STYLE_COLOR, value);
    }

    public final String getColor() {
        return getPropertyValue(STYLE_COLOR);
    }

    public final void clearColor() {
        clearProperty(STYLE_COLOR);
    }

    public final void setCursor(String value) {
        setPropertyValue(STYLE_CURSOR, value);
    }

    public final String getCursor() {
        return getPropertyValue(STYLE_CURSOR);
    }

    public final void clearCursor() {
        clearProperty(STYLE_CURSOR);
    }

    public final void setDisplay(String value) {
        setPropertyValue(STYLE_DISPLAY, value);
    }

    public final String getDisplay() {
        return getPropertyValue(STYLE_DISPLAY);
    }

    public final void clearDisplay() {
        clearProperty(STYLE_DISPLAY);
    }

    public final void setFloat(String value) {
        if (Client.IE) {
            setPropertyValue(STYLE_FLOAT_TRIDENT, value);
        } else {
            setPropertyValue(STYLE_FLOAT, value);
        }
    }

    public final String getFloat() {
        if (Client.IE) {
            return getPropertyValue(STYLE_FLOAT_TRIDENT);
        } else {
            return getPropertyValue(STYLE_FLOAT);
        }
    }

    public final void clearFloat() {
        if (Client.IE) {
            clearProperty(STYLE_FLOAT_TRIDENT);
        } else {
            clearProperty(STYLE_FLOAT);
        }
    }

    public final void setFontSize(String value) {
        setPropertyValue(STYLE_FONT_SIZE, value);
    }

    public final String getFontSize() {
        return getPropertyValue(STYLE_FONT_SIZE);
    }

    public final void clearFontSize() {
        clearProperty(STYLE_FONT_SIZE);
    }

    public final void setFontStyle(String value) {
        setPropertyValue(STYLE_FONT_STYLE, value);
    }

    public final String getFontStyle() {
        return getPropertyValue(STYLE_FONT_STYLE);
    }

    public final void clearFontStyle() {
        clearProperty(STYLE_FONT_STYLE);
    }

    public final void setFontWeight(String value) {
        setPropertyValue(STYLE_FONT_WEIGHT, value);
    }

    public final String getFontWeight() {
        return getPropertyValue(STYLE_FONT_WEIGHT);
    }

    public final void clearFontWeight() {
        clearProperty(STYLE_FONT_WEIGHT);
    }

    public final void setHeight(String value) {
        setPropertyValue(STYLE_HEIGHT, value);
    }

    public final String getHeight() {
        return getPropertyValue(STYLE_HEIGHT);
    }

    public final void clearHeight() {
        clearProperty(STYLE_HEIGHT);
    }

    public final void setLeft(String value) {
        setPropertyValue(STYLE_LEFT, value);
    }

    public final String getLeft() {
        return getPropertyValue(STYLE_LEFT);
    }

    public final void clearLeft() {
        clearProperty(STYLE_LEFT);
    }

    public final void setLineHeight(String value) {
        setPropertyValue(STYLE_LINE_HEIGHT, value);
    }

    public final String getLineHeight() {
        return getPropertyValue(STYLE_LINE_HEIGHT);
    }

    public final void clearLineHeight() {
        clearProperty(STYLE_LINE_HEIGHT);
    }

    public final void setListStyleType(String value) {
        setPropertyValue(STYLE_LIST_STYLE_TYPE, value);
    }

    public final String getListStyleType() {
        return getPropertyValue(STYLE_LIST_STYLE_TYPE);
    }

    public final void clearListStyleType() {
        clearProperty(STYLE_LIST_STYLE_TYPE);
    }

    public final void setMargin(String value) {
        setPropertyValue(STYLE_MARGIN, value);
    }

    public final String getMargin() {
        return getPropertyValue(STYLE_MARGIN);
    }

    public final void clearMargin() {
        clearProperty(STYLE_MARGIN);
    }

    public final void setMarginBottom(String value) {
        setPropertyValue(STYLE_MARGIN_BOTTOM, value);
    }

    public final String getMarginBottom() {
        return getPropertyValue(STYLE_MARGIN_BOTTOM);
    }

    public final void clearMarginBottom() {
        clearProperty(STYLE_MARGIN_BOTTOM);
    }

    public final void setMarginLeft(String value) {
        setPropertyValue(STYLE_MARGIN_LEFT, value);
    }

    public final String getMarginLeft() {
        return getPropertyValue(STYLE_MARGIN_LEFT);
    }

    public final void clearMarginLeft() {
        clearProperty(STYLE_MARGIN_LEFT);
    }

    public final void setMarginRight(String value) {
        setPropertyValue(STYLE_MARGIN_RIGHT, value);
    }

    public final String getMarginRight() {
        return getPropertyValue(STYLE_MARGIN_RIGHT);
    }

    public final void clearMarginRight() {
        clearProperty(STYLE_MARGIN_RIGHT);
    }

    public final void setMarginTop(String value) {
        setPropertyValue(STYLE_MARGIN_TOP, value);
    }

    public final String getMarginTop() {
        return getPropertyValue(STYLE_MARGIN_TOP);
    }

    public final void clearMarginTop() {
        clearProperty(STYLE_MARGIN_TOP);
    }
    //TODO:this needs to revisit
    public final void setOpacity(String value) {
        setPropertyValue(STYLE_OPACITY, value);
    }

    public final String getOpacity() {
        return getPropertyValue(STYLE_OPACITY);
    }

    public final void clearOpacity() {
        clearProperty(STYLE_OPACITY);
    }

    public final void setOutlineColor(String value) {
        setPropertyValue(STYLE_OUTLINE_COLOR, value);
    }

    public final String getOutlineColor() {
        return getPropertyValue(STYLE_OUTLINE_COLOR);
    }

    public final void clearOutlineColor() {
        clearProperty(STYLE_OUTLINE_COLOR);
    }

    public final void setOutlineStyle(String value) {
        setPropertyValue(STYLE_OUTLINE_STYLE, value);
    }

    public final String getOutlineStyle() {
        return getPropertyValue(STYLE_OUTLINE_STYLE);
    }

    public final void clearOutlineStyle() {
        clearProperty(STYLE_OUTLINE_STYLE);
    }

    public final void setOutlineWidth(String value) {
        setPropertyValue(STYLE_OUTLINE_WIDTH, value);
    }

    public final String getOutlineWidth() {
        return getPropertyValue(STYLE_OUTLINE_WIDTH);
    }

    public final void clearOutlineWidth() {
        clearProperty(STYLE_OUTLINE_WIDTH);
    }

    public final void setOverflow(String value) {
        setPropertyValue(STYLE_OVERFLOW, value);
    }

    public final String getOverflow() {
        return getPropertyValue(STYLE_OVERFLOW);
    }

    public final void clearOverflow() {
        clearProperty(STYLE_OVERFLOW);
    }

    public final void setOverflowX(String value) {
        setPropertyValue(STYLE_OVERFLOW_X, value);
    }

    public final String getOverflowX() {
        return getPropertyValue(STYLE_OVERFLOW_X);
    }

    public final void clearOverflowX() {
        clearProperty(STYLE_OVERFLOW_X);
    }

    public final void setOverflowY(String value) {
        setPropertyValue(STYLE_OVERFLOW_Y, value);
    }

    public final String getOverflowY() {
        return getPropertyValue(STYLE_OVERFLOW_Y);
    }

    public final void clearOverflowY() {
        clearProperty(STYLE_OVERFLOW_Y);
    }

    public final void setPadding(String value) {
        setPropertyValue(STYLE_PADDING, value);
    }

    public final String getPadding() {
        return getPropertyValue(STYLE_PADDING);
    }

    public final void clearPadding() {
        clearProperty(STYLE_PADDING);
    }

    public final void setPaddingBottom(String value) {
        setPropertyValue(STYLE_PADDING_BOTTOM, value);
    }

    public final String getPaddingBottom() {
        return getPropertyValue(STYLE_PADDING_BOTTOM);
    }

    public final void clearPaddingBottom() {
        clearProperty(STYLE_PADDING_BOTTOM);
    }

    public final void setPaddingLeft(String value) {
        setPropertyValue(STYLE_PADDING_LEFT, value);
    }

    public final String getPaddingLeft() {
        return getPropertyValue(STYLE_PADDING_LEFT);
    }

    public final void clearPaddingLeft() {
        clearProperty(STYLE_PADDING_LEFT);
    }

    public final void setPaddingRight(String value) {
        setPropertyValue(STYLE_PADDING_RIGHT, value);
    }

    public final String getPaddingRight() {
        return getPropertyValue(STYLE_PADDING_RIGHT);
    }

    public final void clearPaddingRight() {
        clearProperty(STYLE_PADDING_RIGHT);
    }

    public final void setPaddingTop(String value) {
        setPropertyValue(STYLE_PADDING_TOP, value);
    }

    public final String getPaddingTop() {
        return getPropertyValue(STYLE_PADDING_TOP);
    }

    public final void clearPaddingTop() {
        clearProperty(STYLE_PADDING_TOP);
    }

    public final void setPosition(String value) {
        setPropertyValue(STYLE_POSITION, value);
    }

    public final String getPosition() {
        return getPropertyValue(STYLE_POSITION);
    }

    public final void clearPosition() {
        clearProperty(STYLE_POSITION);
    }

    public final void setRight(String value) {
        setPropertyValue(STYLE_RIGHT, value);
    }

    public final String getRight() {
        return getPropertyValue(STYLE_RIGHT);
    }

    public final void clearRight() {
        clearProperty(STYLE_RIGHT);
    }

    public final void setTableLayout(String value) {
        setPropertyValue(STYLE_TABLE_LAYOUT, value);
    }

    public final String getTableLayout() {
        return getPropertyValue(STYLE_TABLE_LAYOUT);
    }

    public final void clearTableLayout() {
        clearProperty(STYLE_TABLE_LAYOUT);
    }

    public final void setTextAlign(String value){
        setPropertyValue(STYLE_TEXT_ALIGN, value);
    }
    public final String getTextAlign(){
        return getPropertyValue(STYLE_TEXT_ALIGN);
    }
    public final void setTextDecoration(String value) {
        setPropertyValue(STYLE_TEXT_DECORATION, value);
    }

    public final String getTextDecoration() {
        return getPropertyValue(STYLE_TEXT_DECORATION);
    }

    public final void clearTextDecoration() {
        clearProperty(STYLE_TEXT_DECORATION);
    }

    public final void setTop(String value) {
        setPropertyValue(STYLE_TOP, value);
    }

    public final String getTop() {
        return getPropertyValue(STYLE_TOP);
    }

    public final void clearTop() {
        clearProperty(STYLE_TOP);
    }

    
    public final void setVerticalAlign(String value) {
        setPropertyValue(STYLE_VERTICAL_ALIGN, value);
    }

    public final String getVerticalAlign() {
        return getPropertyValue(STYLE_VERTICAL_ALIGN);
    }

    public final void clearVerticalAlign() {
        clearProperty(STYLE_VERTICAL_ALIGN);
    }

    public final void setVisibility(String value) {
        setPropertyValue(STYLE_VISIBILITY, value);
    }

    public final String getVisibility() {
        return getPropertyValue(STYLE_VISIBILITY);
    }

    public final void clearVisibility() {
        clearProperty(STYLE_VISIBILITY);
    }

    public final void setWidth(String value) {
        setPropertyValue(STYLE_WIDTH, value);
    }

    public final String getWidth() {
        return getPropertyValue(STYLE_WIDTH);
    }

    public final void clearWidth() {
        clearProperty(STYLE_WIDTH);
    }

    public final void setZIndex(int value) {
        this.setPropertyValue(STYLE_Z_INDEX, "" + value);
    }

    public final String getZIndex() {
        return this.getPropertyValue(STYLE_Z_INDEX);
    }

    public final void clearZIndex() {
        this.clearProperty(STYLE_Z_INDEX);
    }

    public native void setCssText(String text)/*-{
    this['cssText']=text;
    }-*/;

    public native void addCssText(String text)/*-{
    this['cssText']+=";"+text;
    }-*/;

    public native String getCssText()/*-{
    return this.cssText;
    }-*/;

    public native void clearProperty(String name) /*- {
    this[name]="";
    } -*/;

    public native String getPropertyValue(String name) /*- {
    return this[name];
    } -*/;

    public native void setPropertyValue(String name, String value) /*- {
    this[name]=value;
    } -*/;
}

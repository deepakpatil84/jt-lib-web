/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class TableElement extends Element {

    public static final String TAG = "table";

    /**
     *
     */
    protected TableElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(
		        TableElement.TAG) : false;
	}

	public static TableElement as(Object elm) {
		
		if (!((Element)elm).getTagName().equalsIgnoreCase(TableElement.TAG)) {
			throw new ClassCastException("Can not covert to TableElement");
		}
		return (TableElement) elm;
	}
    /**
     *
     * @return
     */
    @NoJavaScript
    public final native TableCaptionElement createCaption();

    /**
     *
     * @return
     */
    @NoJavaScript
    public final native TableSectionElement createTFoot();

    /**
     *
     * @return
     */
    @NoJavaScript
    public final native TableSectionElement createTHead();

    /**
     *
     */
    @NoJavaScript
    public final native void deleteCaption();

    /**
     *
     * @param index
     */
    @NoJavaScript
    public final native void deleteRow(int index);

    /**
     *
     */
    @NoJavaScript
    public final native void deleteTFoot();

    /**
     *
     */
    @NoJavaScript
    public final native void deleteTHead();

    /**
     *
     * @return
     */

    public final native String getBorder() /*-{
    return this.border;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableCaptionElement getCaption() /*-{
    return this.caption;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCellPadding() /*-{
    return this.cellPadding;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCellSpacing() /*-{
    return this.cellSpacing;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getFrame() /*-{
    return this.frame;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableRowElement[] getRows() /*-{
    return this.rows;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getRules() /*-{
    return this.rules;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableSectionElement[] getTBodies() /*-{
    return this.tBodies;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableSectionElement getTFoot() /*-{
    return this.tFoot;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableSectionElement getTHead() /*-{
    return this.tHead;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getWidth() /*-{
    return this.width;
    }-*/;

    /**
     *
     * @param index
     * @return
     */
    @NoJavaScript
    public final native TableRowElement insertRow(int index);

    /**
     *
     * @param border
     */
    public final native void setBorder(int border) /*-{
    this.border = border;
    }-*/;

    /**
     *
     * @param caption
     */
    public final native void setCaption(TableCaptionElement caption) /*-{
    this.caption = caption;
    }-*/;

    /**
     *
     * @param cellPadding
     */
    public final native void setCellPadding(int cellPadding) /*-{
    this.cellPadding = cellPadding;
    }-*/;

    /**
     *
     * @param cellSpacing
     */
    public final native void setCellSpacing(int cellSpacing) /*-{
    this.cellSpacing = cellSpacing;
    }-*/;

    /**
     *
     * @param frame
     */
    public final native void setFrame(String frame) /*-{
    this.frame = frame;
    }-*/;

    /**
     *
     * @param rules
     */
    public final native void setRules(String rules) /*-{
    this.rules = rules;
    }-*/;

    /**
     *
     * @param tFoot
     */
    public final native void setTFoot(TableSectionElement tFoot) /*-{
    this.tFoot = tFoot;
    }-*/;

    /**
     *
     * @param tHead
     */
    public final native void setTHead(TableSectionElement tHead) /*-{
    this.tHead = tHead;
    }-*/;

    /**
     *
     * @param width
     */
    public final native void setWidth(String width) /*-{
    this.width = width;
    }-*/;
}

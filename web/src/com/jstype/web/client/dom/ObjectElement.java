/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class ObjectElement extends Element {

    public static final String TAG = "object";

    /**
     *
     */
    protected ObjectElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    
    public static ObjectElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(ObjectElement.TAG)){
    		throw new ClassCastException("Can not covert to ObjectElement");
    	}
    	return (ObjectElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getCode() /*-{
    return this.code;
    }-*/;

    /**
     *
     * @return
     */
    public final native Document getContentDocument() /*-{
    return this.contentDocument;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getData() /*-{
    return this.data;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHeight() /*-{
    return this.height;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getWidth() /*-{
    return this.width;
    }-*/;

    /**
     *
     * @param code
     */
    public final native void setCode(String code) /*-{
    this.code = code;
    }-*/;

    /**
     *
     * @param data
     */
    public final native void setData(String data) /*-{
    this.data = data;
    }-*/;

    /**
     *
     * @param height
     */
    public final native void setHeight(String height) /*-{
    this.height = height;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param type
     */
    public final native void setType(String type) /*-{
    this.type = type;
    }-*/;

    /**
     *
     * @param useMap
     */
    public final native void setUseMap(boolean useMap) /*-{
    this.useMap = useMap;
    }-*/;

    /**
     *
     * @param width
     */
    public final native void setWidth(String width) /*-{
    this.width = width;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getUseMap() /*-{
    return !!this.useMap;
    }-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.core.JSObject;
import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class Node extends JSObject{

    /**
     *
     */
    public static final int ELEMENT_NODE = 1;
    /**
     *
     */
    public static final int ATTRIBUTE_NODE = 2;
    /**
     *
     */
    public static final int TEXT_NODE = 3;
    /**
     *
     */
    public static final int COMMENT_NODE = 8;
    /**
     *
     */
    public static final int DOCUMENT_NODE = 9;

    /**
     *
     */
    protected Node() {
    }

    /**
     *
     * @return
     */
    public final native String getNodeName()/*- {
    return this.nodeName;
    } -*/;

    /**
     *
     * @return
     */
    public final native int getNodeType()/*- {
    return this.nodeType;
    } -*/;

    /**
     *
     * @return
     */
    public final native String getNodeValue()/*- {
    return this.nodeValue;
    } -*/;

    /**
     *
     * @param value
     * @return
     */
    public final native Node setNodeValue(String value)/*- {
    this.nodeValue=value;
    } -*/;

    /**
     *
     * @return
     */
    public final native int getChildCount()/*- {
    return this.childNodes.length;
    } -*/;

    /**
     *
     * @return
     */
    public final native Node[] getChildNodes()/*- {
    return this.childNodes;
    } -*/;

    public final native Node getChildAt(int aIndex)/*- {
    if(aIndex>=0 && aIndex<this.childNodes.length) return this.childNodes[aIndex];
    return null;
    } -*/;

    /**
     *
     * @return
     */
    public final native Node getFirstChild()/*- {
    return this.firstChild;
    } -*/;

    /**
     *
     * @return
     */
    public final native Node getLastChild()/*- {
    return this.lastChild;
    } -*/;

    /**
     *
     * @return
     */
    @NoJavaScript
    public final native boolean hasChildNodes();

    /**
     *
     * @param newChild
     * @param oldChild
     * @return
     */
    @NoJavaScript
    public final native Node insertBefore(Node newChild, Node oldChild);

    /**
    * Will return the Node only if inserted successfully else it will return null
    * @param pos position at which we want to insert 
    * @param child node to insert
    * @return
    */   
   public final Node insertChildAt(int pos,Node child){
	   Node existing=getChildAt(pos);
	   if(existing != null ){
		   return insertBefore(child, getChildAt(pos));
	   }else{
		   if(pos <= 0){
			   return appendChild(child);
		   }
	   }
	   return null;
   }
    /**
     *
     * @param nodeToRemove
     * @return
     */
    @NoJavaScript
    public final native Node removeChild(Node nodeToRemove);
    
    
    /**
    *
    * @param index position of node to remove
    * @return
    */
   @NoJavaScript
   public final Node removeChildAt(int index){
	   return removeChild(getChildAt(index));
   }
    

    /**
     *
     */
    public final void removeFromParent() {
        Node parent = this.getParentNode();
        if (parent != null) {
            parent.removeChild(this);
        }
    }

    /**
     *
     * @param newChild
     * @param oldChild
     * @return
     */
    @NoJavaScript
    public final native Node replaceChild(Node newChild, Node oldChild);

    /**
     *
     * @return
     */
    public final native Node getNextSibling()/*- {
    return this.nextSibling;
    } -*/;
    //public final native Node cloneNode(boolean aEverything);

    /**
     *
     * @return
     */
    public final native Node getPreviousSibling()/*- {
    return this.previousSibling;
    } -*/;

    /**
     *
     * @param child
     * @return
     */
    @NoJavaScript
    public final native Node appendChild(Node child);

    /**
     *
     * @return
     */
    public final native Document getOwnerDocument()/*- {
    return this.ownerDocument;
    } -*/;

    /**
     *
     * @return
     */
    public final native Node getParentNode()/*- {
    return this.parentNode;
    } -*/;

    /**
     * obj.innerHTML=""; does not work in IE6/9 it deletes the Element nodes
     * @return
     */
    public final native void clearChilds()/*- {
    try{
    
    var len = this.childNodes.length;
    for(var i = len -1 ; i >=0 ; i--){
    this.removeChild(this.childNodes[i]);
    }
    }catch(e){}
    } -*/;
}

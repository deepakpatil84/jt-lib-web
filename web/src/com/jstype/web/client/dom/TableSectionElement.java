/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class TableSectionElement extends Element {

    public static final String TAG_TBODY = "tbody";
    public static final String TAG_TFOOT = "tfoot";
    public static final String TAG_THEAD = "thead";

    /**
     *
     */
    protected TableSectionElement() {
    }
    
    public static boolean is(Object elm) {
    	boolean rvalue=false;
    	if(Element.is(elm)){
    		String tagName=((Element)elm).getTagName();
    		if(tagName.equalsIgnoreCase(TAG_TBODY) || tagName.equalsIgnoreCase(TAG_TFOOT) || tagName.equalsIgnoreCase(TAG_THEAD)){
    			rvalue = true;
    		}
    	}
		return rvalue;
	}

	public static TableSectionElement as(Object elm) {
		String s=((Element)elm).getTagName();
		if (!(s.equalsIgnoreCase(TableSectionElement.TAG_TBODY)
		        || s.equalsIgnoreCase(TableSectionElement.TAG_TFOOT) 
		        || s.equalsIgnoreCase(TableSectionElement.TAG_THEAD))) {
			throw new ClassCastException(
			        "Can not covert to TableSectionElement");
		}
		return (TableSectionElement) elm;
	}

    /**
     *
     * @param index
     */
    @NoJavaScript
    public final native void deleteRow(int index);

    /**
     *
     * @return
     */
    public final native String getAlign() /*-{
    return this.align;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCh() /*-{
    return this.ch;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getChOff() /*-{
    return this.chOff;
    }-*/;

    /**
     *
     * @return
     */
    public final native TableRowElement[] getRows() /*-{
    return this.rows;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getVAlign() /*-{
    return this.vAlign;
    }-*/;

    /**
     *
     * @param index
     * @return
     */
    @NoJavaScript
    public final native TableRowElement insertRow(int index);

    /**
     *
     * @param align
     */
    public final native void setAlign(String align) /*-{
    this.align = align;
    }-*/;

    /**
     *
     * @param ch
     */
    public final native void setCh(String ch) /*-{
    this.ch = ch;
    }-*/;

    /**
     *
     * @param chOff
     */
    public final native void setChOff(String chOff) /*-{
    this.chOff = chOff;
    }-*/;

    /**
     *
     * @param vAlign
     */
    public final native void setVAlign(String vAlign) /*-{
    this.vAlign = vAlign;
    }-*/;
}

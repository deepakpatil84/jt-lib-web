/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class StyleElement extends Element {

    public static final String TAG = "style";

    /**
     *
     */
    protected StyleElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(
		        StyleElement.TAG) : false;
	}

	public static StyleElement as(Object elm) {
		
		if (!((Element)elm).getTagName().equalsIgnoreCase(StyleElement.TAG)) {
			throw new ClassCastException("Can not covert to StyleElement");
		}
		return (StyleElement) elm;
	}
    /**
     *
     * @return
     */
    public final native String getCssText() /*-{
    return this.cssText;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getMedia() /*-{
    return this.media;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @param cssText
     */
    public final native void setCssText(String cssText) /*-{
    this.cssText = cssText;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param media
     */
    public final native void setMedia(String media) /*-{
    this.media = media;
    }-*/;

    /**
     *
     * @param type
     */
    public final native void setType(String type) /*-{
    this.type = type;
    }-*/;
}

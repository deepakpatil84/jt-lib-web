/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class ButtonElement extends Element {

    public static final String TAG = "button";

    /**
     *
     */
    protected ButtonElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static ButtonElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(ButtonElement.TAG)){
    		throw new ClassCastException("Can not covert to ButtonElement");
    	}
    	return (ButtonElement)elm;
    }
    /**
     *
     */
    @NoJavaScript 
    public final native void click();
   

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getDisabled() /*-{
    return this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getValue() /*-{
    return this.value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(String disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param value
     */
    public final native void setValue(String value) /*-{
    this.value = value;
    }-*/;
}

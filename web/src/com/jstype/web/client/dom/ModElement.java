/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class ModElement extends Element {

    public static final String TAG_INS = "ins";
    public static final String TAG_DEL = "del";

    /**
     *
     */
    protected ModElement() {
    }
    

	public static boolean is(Object elm) {
		return Element.is(elm) ? (((Element) elm).getTagName().equals(
		        TAG_INS) || ((Element) elm).getTagName().equals(TAG_DEL))
		        : false;
	}
    
    public static ModElement as(Object elm){
    	
    	if(!( ((Element)elm).getTagName().equals(ModElement.TAG_INS) ||((Element)elm).getTagName().equals(ModElement.TAG_DEL))){
    		throw new ClassCastException("Can not covert to ModElement");
    	}
    	return (ModElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getCite() /*-{
    return this.cite;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getDateTime() /*-{
    return this.dateTime;
    }-*/;

    /**
     *
     * @param cite
     */
    public final native void setCite(String cite) /*-{
    this.cite = cite;
    }-*/;

    /**
     *
     * @param dateTime
     */
    public final native void setDateTime(String dateTime) /*-{
    this.dateTime = dateTime;
    }-*/;
}

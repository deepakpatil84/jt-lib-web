/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class TableColElement extends Element {

    public static final String TAG_COL = "col";
    public static final String TAG_COLGROUP = "colgroup";

    /**
     *
     */
    protected TableColElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? (((Element) elm).getTagName().equalsIgnoreCase(
		        TableColElement.TAG_COL) || ((Element) elm).getTagName()
		        .equalsIgnoreCase(TableColElement.TAG_COLGROUP)) : false;
	}
	public static TableColElement as(Object elm) {
		
		if (!(((Element)elm).getTagName().equalsIgnoreCase(TableColElement.TAG_COL) || ((Element)elm).getTagName().equalsIgnoreCase(TableColElement.TAG_COLGROUP))) {
			throw new ClassCastException("Can not covert to TableColElement");
		}
		return (TableColElement) elm;
	}
    /**
     *
     * @return
     */
    public final native String getAlign() /*-{
    return this.align;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCh() /*-{
    return this.ch;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getChOff() /*-{
    return this.chOff;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getSpan() /*-{
    return this.span;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getVAlign() /*-{
    return this.vAlign;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getWidth() /*-{
    return this.width;
    }-*/;

    /**
     *
     * @param align
     */
    public final native void setAlign(String align) /*-{
    this.align = align;
    }-*/;

    /**
     *
     * @param ch
     */
    public final native void setCh(String ch) /*-{
    this.ch = ch;
    }-*/;

    /**
     *
     * @param chOff
     */
    public final native void setChOff(String chOff) /*-{
    this.chOff = chOff;
    }-*/;

    /**
     *
     * @param span
     */
    public final native void setSpan(int span) /*-{
    this.span = span;
    }-*/;

    /**
     *
     * @param vAlign
     */
    public final native void setVAlign(String vAlign) /*-{
    this.vAlign = vAlign;
    }-*/;

    /**
     *
     * @param width
     */
    public final native void setWidth(String width) /*-{
    this.width = width;
    }-*/;
}

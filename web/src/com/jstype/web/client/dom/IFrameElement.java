/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;
import com.jstype.web.client.event.dom.LoadHandler;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.HasLoadHandlers;

@NativeNonInstanciable
public class IFrameElement extends Element implements HasLoadHandlers{

    public static final String TAG = "iframe";

    /**
     *
     */
    protected IFrameElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static IFrameElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(IFrameElement.TAG)){
    		throw new ClassCastException("Can not covert to IFrameElement");
    	}
    	return (IFrameElement)elm;
    }
    /**
     *
     * @return
     */
    public final native Document getContentDocument() /*-{
    return this.contentWindow.document;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getFrameBorder() /*-{
    return this.frameBorder;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getMarginHeight() /*-{
    return this.marginHeight;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getMarginWidth() /*-{
    return this.marginWidth;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getScrolling() /*-{
    return this.scrolling;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getSrc() /*-{
    return this.src;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isNoResize() /*-{
    return !!this.noResize;
    }-*/;

    /**
     *
     * @param frameBorder
     */
    public final native void setFrameBorder(int frameBorder) /*-{
    this.frameBorder = frameBorder;
    }-*/;

    /**
     *
     * @param marginHeight
     */
    public final native void setMarginHeight(int marginHeight) /*-{
    this.marginHeight = marginHeight;
    }-*/;

    /**
     *
     * @param marginWidth
     */
    public final native void setMarginWidth(int marginWidth) /*-{
    this.marginWidth = marginWidth;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param noResize
     */
    public final native void setNoResize(boolean noResize) /*-{
    this.noResize = noResize;
    }-*/;

    /**
     *
     * @param scrolling
     */
    public final native void setScrolling(String scrolling) /*-{
    this.scrolling = scrolling;
    }-*/;

    /**
     *
     * @param src
     */
    public final native void setSrc(String src) /*-{
    this.src = src;
    }-*/;

    public HandlerRegistration addLoadHandler(LoadHandler handler){
        return this.addEventHandler("load", handler);
    }
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class HeadingElement extends Element {

	public static final String TAG_H1 = "h1";
	public static final String TAG_H2 = "h2";
	public static final String TAG_H3 = "h3";
	public static final String TAG_H4 = "h4";
	public static final String TAG_H5 = "h5";
	public static final String TAG_H6 = "h6";

	/**
     *
     */
	protected HeadingElement() {
	}

	public static boolean is(Object elm) {
		boolean rvalue = false;
		if (Element.is(elm)) {
			String tagName = ((Element) elm).getTagName();
			if (tagName.equalsIgnoreCase(TAG_H1) || tagName.equalsIgnoreCase(TAG_H2)
			        || tagName.equalsIgnoreCase(TAG_H3) || tagName.equalsIgnoreCase(TAG_H4)
			        || tagName.equalsIgnoreCase(TAG_H5) || tagName.equalsIgnoreCase(TAG_H6)) {
				rvalue = true;
			}
		}
		return rvalue;
	}
	public static HeadingElement as(Object elm) {
		Element elm1=(Element)elm;
		if (!(elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H1)
		        || elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H2)
		        || elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H3)
		        || elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H4)
		        || elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H5) 
		        || elm1.getTagName().equalsIgnoreCase(HeadingElement.TAG_H6))) {
			throw new ClassCastException("Can not covert to HeadingElement");
		}
		return (HeadingElement) elm;
	}
}

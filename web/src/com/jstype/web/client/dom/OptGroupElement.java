/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class OptGroupElement extends Element {

    public static final String TAG = "optgroup";

    /**
     *
     */
    protected OptGroupElement() {
    }
    
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static OptGroupElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(OptGroupElement.TAG)){
    		throw new ClassCastException("Can not covert to OptGroupElement");
    	}
    	return (OptGroupElement)elm;
    }

    /**
     *
     * @return
     */
    public final native boolean getDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getLabel() /*-{
    return this.label;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(String disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param label
     */
    public final native void setLabel(String label) /*-{
    this.label = label;
    }-*/;
}

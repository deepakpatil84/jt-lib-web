/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class TextAreaElement extends Element {

    public static final String TAG = "textarea";

    /**
     *
     */
    protected TextAreaElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(
		        TextAreaElement.TAG) : false;
	}


	public static TextAreaElement as(Object elm) {
		
		if (!((Element)elm).getTagName().equalsIgnoreCase(TextAreaElement.TAG)) {
			throw new ClassCastException("Can not covert to TextAreaElement");
		}
		return (TextAreaElement) elm;
	}

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getCols() /*-{
    return this.cols;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getDefaultValue() /*-{
    return this.defaultValue;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean getReadOnly() /*-{
    return !!this.readOnly;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getRows() /*-{
    return this.rows;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getType() /*-{
    return this.type;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getValue() /*-{
    return this.value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isReadOnly() /*-{
    return !!this.readOnly;
    }-*/;

    /**
     *
     */
    public final native void select() /*-{
    this.select();
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param cols
     */
    public final native void setCols(int cols) /*-{
    this.cols = cols;
    }-*/;

    /**
     *
     * @param defaultValue
     */
    public final native void setDefaultValue(String defaultValue) /*-{
    this.defaultValue = defaultValue;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    this.disabled = disabled;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param readOnly
     */
    public final native void setReadOnly(boolean readOnly) /*-{
    this.readOnly = readOnly;
    }-*/;

    /**
     *
     * @param rows
     */
    public final native void setRows(int rows) /*-{
    this.rows = rows;
    }-*/;

    /**
     *
     * @param value
     */
    public final native void setValue(String value) /*-{
    this.value = value;
    }-*/;
}

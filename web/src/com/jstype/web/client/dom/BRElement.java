/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class BRElement extends Element {

    public static final String TAG = "br";

    /**
     *
     */
    protected BRElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static BRElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(BRElement.TAG)){
    		throw new ClassCastException("Can not covert to BRElement");
    	}
    	return (BRElement)elm;
    }
}

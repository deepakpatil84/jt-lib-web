/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class MetaElement extends Element {

    public static final String TAG = "meta";

    /**
     *
     */
    protected MetaElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static MetaElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(MetaElement.TAG)){
    		throw new ClassCastException("Can not covert to MetaElement");
    	}
    	return (MetaElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getContent() /*-{
    return this.content;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHttpEquiv() /*-{
    return this.httpEquiv;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @param content
     */
    public final native void setContent(String content) /*-{
    this.content = content;
    }-*/;

    /**
     *
     * @param httpEquiv
     */
    public final native void setHttpEquiv(String httpEquiv) /*-{
    this.httpEquiv = httpEquiv;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;
}

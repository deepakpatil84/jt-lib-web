/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;
import com.jstype.web.client.event.Event;
import com.jstype.web.client.event.EventHandler;
import com.jstype.web.client.event.EventManager;
import com.jstype.web.client.event.HandlerRegistration;
import com.jstype.web.client.event.dom.BlurEvent;
import com.jstype.web.client.event.dom.BlurHandler;
import com.jstype.web.client.event.dom.ChangeEvent;
import com.jstype.web.client.event.dom.ChangeHandler;
import com.jstype.web.client.event.dom.ClickEvent;
import com.jstype.web.client.event.dom.ClickHandler;
import com.jstype.web.client.event.dom.ContextMenuEvent;
import com.jstype.web.client.event.dom.ContextMenuHandler;
import com.jstype.web.client.event.dom.CustomEventHandler;
import com.jstype.web.client.event.dom.DoubleClickEvent;
import com.jstype.web.client.event.dom.DoubleClickHandler;
import com.jstype.web.client.event.dom.FocusEvent;
import com.jstype.web.client.event.dom.FocusHandler;
import com.jstype.web.client.event.dom.HasAllKeyHandlers;
import com.jstype.web.client.event.dom.HasAllMouseHandlers;
import com.jstype.web.client.event.dom.HasBlurHandlers;
import com.jstype.web.client.event.dom.HasChangeHandlers;
import com.jstype.web.client.event.dom.HasClickHandlers;
import com.jstype.web.client.event.dom.HasContextMenuHandlers;
import com.jstype.web.client.event.dom.HasDoubleClickHandlers;
import com.jstype.web.client.event.dom.HasFocusHandlers;
import com.jstype.web.client.event.dom.HasLoadHandlers;
import com.jstype.web.client.event.dom.KeyDownEvent;
import com.jstype.web.client.event.dom.KeyDownHandler;
import com.jstype.web.client.event.dom.KeyPressEvent;
import com.jstype.web.client.event.dom.KeyPressHandler;
import com.jstype.web.client.event.dom.KeyUpEvent;
import com.jstype.web.client.event.dom.KeyUpHandler;
import com.jstype.web.client.event.dom.LoadEvent;
import com.jstype.web.client.event.dom.LoadHandler;
import com.jstype.web.client.event.dom.MouseDownEvent;
import com.jstype.web.client.event.dom.MouseDownHandler;
import com.jstype.web.client.event.dom.MouseMoveEvent;
import com.jstype.web.client.event.dom.MouseMoveHandler;
import com.jstype.web.client.event.dom.MouseOutEvent;
import com.jstype.web.client.event.dom.MouseOutHandler;
import com.jstype.web.client.event.dom.MouseOverEvent;
import com.jstype.web.client.event.dom.MouseOverHandler;
import com.jstype.web.client.event.dom.MouseUpEvent;
import com.jstype.web.client.event.dom.MouseUpHandler;

public class Document extends Node implements HasAllKeyHandlers,
        HasAllMouseHandlers, HasBlurHandlers, HasChangeHandlers,
        HasClickHandlers, HasContextMenuHandlers, HasDoubleClickHandlers,
        HasFocusHandlers, HasLoadHandlers {
	/* should not be instantiated */

	/**
     *
     */
	protected Document() {
	}

	/**
	 * 
	 * @return
	 */
	public final native Document getDocumentInstance()
	/*- {
		return this;
	} -*/;

	/**
	 * 
	 * @param tagname
	 * @return
	 */
	@NoJavaScript
	public final native Element createElement(String tagname);

	/**
	 * 
	 * @param text
	 * @return
	 */
	@NoJavaScript
	public final native Element createTextNode(String text);

	/**
	 * 
	 * @return
	 */
	public final native BodyElement getBodyElement()
	/*- {
		return this.body;
	} -*/;

	/**
	 * 
	 * @return
	 */
	public final native String getCompatMode()
	/*- {
		return this.compatMode;
	} -*/;

	/**
	 * 
	 * @return
	 */
	public final native String getDomain()
	/*- {
		return this.domain;
	} -*/;

	/**
	 * 
	 * @param id
	 * @return
	 */
	@NoJavaScript
	public final native Element getElementById(String id);

	/**
	 * 
	 * @return
	 */
	public final native String getReferrer()
	/*- {
		return this.referrer;
	} -*/;

	/**
	 * 
	 * @return
	 */
	public final native String getURL()
	/*- {
		return this.URL;
	} -*/;

	/**
	 * 
	 * @param node
	 * @param all
	 */
	@NoJavaScript
	public final native void importNode(Node node, boolean all);

	public final native void setTitle(String title)
	/*-{
	    this.title=title;
	}-*/;

	public final native String getTitle()
	/*-{
	    return this.title;
	}-*/;

	public final native boolean queryCommandBoolean(String cmd)
	/*-{
	    return !!this.queryCommandValue(cmd);
	} -*/;

	public final native String queryCommandString(String cmd)
	/*-{
	    return this.queryCommandValue(cmd);
	} -*/;

	@NoJavaScript
	public final native boolean queryCommandState(String cmd);

	@NoJavaScript
	public final native void execCommand(String cmd, boolean b, String param);

	public final native void enableEditing()
	/*- {
	    if(this.designMode){
	       this.designMode='On';
	    }else{
	    }
	}-*/;

	public HandlerRegistration addLoadHandler(LoadHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public final native int getClientHeight()
	/*- {
		return this.clientHeight;
	} -*/;

	public final native int getClientWidth()
	/*- {
		return this.clientWidth;
	} -*/;

	private EventManager __eventmanager;

	private void handleNativeEvent(NativeEvent nativeEvent) {
		Event e = createNativeEvent(nativeEvent);
		if (e != null) {
			__eventmanager.fireEvent(nativeEvent.getType(), e);
		} else {
			__eventmanager.fireCustomEvent(nativeEvent);
		}
	}

	private void registerEventListener(String name) {
		if (false) {
			// just to mark it as used
			// TODO:remove
			this.handleNativeEvent(null);
		}
		// if(Client.IE && (!Client.IE_9_OR_ABOVE)){
		if (isAttachEvent(this)) {
			__registerEventIE(this, name);
		} else {
			__registerEvent(this, name);
		}
	}

	private static native boolean isAttachEvent(Object src)
	/*-{
		return !!src.attachEvent
	}-*/;

	private void unregisterEventListener(String name) {

		// if(Client.IE && (!Client.IE_9_OR_ABOVE)){
		if (isAttachEvent(this)) {
			__unregisterEventIE(this, name);
		} else {
			__unregisterEvent(this, name);
		}
	}

	private native void __registerEvent(Document src, String name)
	/*-{
	    src.addEventListener(name,this.__eh,false);
	}-*/;

	private native void __unregisterEvent(Document src, String name)
	/*-{
	    src.removeEventListener(name,this.__eh,false);
	}-*/;

	private native void __registerEventIE(Document src, String name)
	/*-{
	    src.attachEvent("on"+name,this.__eh);
	}-*/;

	private native void __unregisterEventIE(Document src, String name)
	/*-{
	    src.detachEvent("on"+name,this.__eh);
	}-*/;

	// TODO:i have used a trick here in order to support IE6 , if you are planning to access DOM classes in native mode
	// please dont do that
	// will update on this soon
	private native void assignEventManagerRef(EventManager ref)
	/*-{
	       this.__em=ref;
	       var s=this;
	       this.__eh=function(e){ 
	       	s.@com.jstype.web.client.dom.Document::handleNativeEvent(Lcom/jstype/web/client/dom/NativeEvent;)(e);
	       };

	}-*/;

	protected HandlerRegistration addEventHandler(String eventName,
	        final EventHandler handler) {
		if (this.__eventmanager == null) {
			this.__eventmanager = new EventManager();
			this.assignEventManagerRef(__eventmanager);
		}
		final String type = eventName != null ? eventName
		        : getEventHandlerType(handler);
		registerEventListener(type);
		this.__eventmanager.addEventHandler(type, handler);
		return new HandlerRegistration() {
			public void removeHandler() {
				__eventmanager.removeEventHandler(type, handler);
				if (!__eventmanager.hasEventHandlersForType(type)) {
					unregisterEventListener(type);
				}
			}
		};
	}

	public void removeEventHandler(EventHandler handler) {
		if (__eventmanager != null) {
			String type = getEventHandlerType(handler);
			__eventmanager.removeEventHandler(type, handler);
			if (!__eventmanager.hasEventHandlersForType(type)) {
				unregisterEventListener(type);
			}
		}
	}

	public boolean hasEventHandlersForType(String type) {
		return __eventmanager == null ? false : __eventmanager
		        .hasEventHandlersForType(type);
	}

	public void fireEvent(Event<?> event) {
		if (this.__eventmanager != null) {
			this.__eventmanager.fireEvent(getEventType(event), event);
		}
	}

	public HandlerRegistration addCustomEventHandler(String eventName,
	        CustomEventHandler handler) {
		return this.addEventHandler(eventName, handler);
	}

	public HandlerRegistration addKeyDownHandler(KeyDownHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addKeyUpHandler(KeyUpHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addKeyPressHandler(KeyPressHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addMouseDownHandler(MouseDownHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addMouseMoveHandler(MouseMoveHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addMouseOutHandler(MouseOutHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addMouseOverHandler(MouseOverHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addMouseUpHandler(MouseUpHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addBlurHandler(BlurHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addChangeHandler(ChangeHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addClickHandler(ClickHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addContextMenuHandler(ContextMenuHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addDoubleClickHandler(DoubleClickHandler handler) {
		return this.addEventHandler(null, handler);
	}

	public HandlerRegistration addFocusHandler(FocusHandler handler) {
		return this.addEventHandler(null, handler);
	}

	private static Event createNativeEvent(NativeEvent nativeEvent) {
		String name = nativeEvent.getType();
		if (name.equalsIgnoreCase(BlurEvent.NAME)) {
			return new BlurEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(ChangeEvent.NAME)) {
			return new ChangeEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(ClickEvent.NAME)) {
			return new ClickEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(ContextMenuEvent.NAME)) {
			return new ContextMenuEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(DoubleClickEvent.NAME)) {
			return new DoubleClickEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(FocusEvent.NAME)) {
			return new FocusEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(KeyDownEvent.NAME)) {
			return new KeyDownEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(KeyPressEvent.NAME)) {
			return new KeyPressEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(KeyUpEvent.NAME)) {
			return new KeyUpEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(LoadEvent.NAME)) {
			return new LoadEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(MouseDownEvent.NAME)) {
			return new MouseDownEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(MouseMoveEvent.NAME)) {
			return new MouseMoveEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(MouseOutEvent.NAME)) {
			return new MouseOutEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(MouseOverEvent.NAME)) {
			return new MouseOverEvent(nativeEvent);
		}
		if (name.equalsIgnoreCase(MouseUpEvent.NAME)) {
			return new MouseUpEvent(nativeEvent);
		}

		// TODO:implement for other events
		return null;
	}

	public static String getEventHandlerType(EventHandler handler) {
		if (handler instanceof BlurHandler) {
			return BlurEvent.NAME;
		}
		if (handler instanceof ChangeHandler) {
			return ChangeEvent.NAME;
		}
		if (handler instanceof ClickHandler) {
			return ClickEvent.NAME;
		}
		if (handler instanceof ContextMenuHandler) {
			return ContextMenuEvent.NAME;
		}
		if (handler instanceof DoubleClickHandler) {
			return DoubleClickEvent.NAME;
		}
		if (handler instanceof FocusHandler) {
			return FocusEvent.NAME;
		}
		if (handler instanceof KeyDownHandler) {
			return KeyDownEvent.NAME;
		}
		if (handler instanceof KeyPressHandler) {
			return KeyPressEvent.NAME;
		}
		if (handler instanceof KeyUpHandler) {
			return KeyUpEvent.NAME;
		}
		if (handler instanceof LoadHandler) {
			return LoadEvent.NAME;
		}
		if (handler instanceof MouseDownHandler) {
			return MouseDownEvent.NAME;
		}
		if (handler instanceof MouseMoveHandler) {
			return MouseMoveEvent.NAME;
		}
		if (handler instanceof MouseOutHandler) {
			return MouseOutEvent.NAME;
		}
		if (handler instanceof MouseOverHandler) {
			return MouseOverEvent.NAME;
		}
		if (handler instanceof MouseUpHandler) {
			return MouseUpEvent.NAME;
		}
		return null;// throw exception over here
	}

	private static String getEventType(Event event) {
		if (event instanceof BlurEvent) {
			return BlurEvent.NAME;
		}
		if (event instanceof ChangeEvent) {
			return ChangeEvent.NAME;
		}
		if (event instanceof ClickEvent) {
			return ClickEvent.NAME;
		}
		if (event instanceof ContextMenuEvent) {
			return ContextMenuEvent.NAME;
		}
		if (event instanceof DoubleClickEvent) {
			return DoubleClickEvent.NAME;
		}
		if (event instanceof FocusEvent) {
			return FocusEvent.NAME;
		}
		if (event instanceof KeyDownEvent) {
			return KeyDownEvent.NAME;
		}
		if (event instanceof KeyPressEvent) {
			return KeyPressEvent.NAME;
		}
		if (event instanceof KeyUpEvent) {
			return KeyUpEvent.NAME;
		}
		if (event instanceof LoadEvent) {
			return LoadEvent.NAME;
		}
		if (event instanceof MouseDownEvent) {
			return MouseDownEvent.NAME;
		}
		if (event instanceof MouseMoveEvent) {
			return MouseMoveEvent.NAME;
		}
		if (event instanceof MouseOutEvent) {
			return MouseOutEvent.NAME;
		}
		if (event instanceof MouseOverEvent) {
			return MouseOverEvent.NAME;
		}
		if (event instanceof MouseUpEvent) {
			return MouseUpEvent.NAME;
		}
		return null;// throw exception over here
	}

}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class FrameElement extends Element {

    public static final String TAG = "frame";

    /**
     *
     */
    protected FrameElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    
    public static FrameElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(FrameElement.TAG)){
    		throw new ClassCastException("Can not covert to FrameElement");
    	}
    	return (FrameElement)elm;
    }

    /**
     *
     * @return
     */
    public final native Document getContentDocument() /*-{
    return this.contentWindow.document;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getFrameBorder() /*-{
    return this.frameBorder;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getLongDesc() /*-{
    return this.longDesc;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getMarginHeight() /*-{
    return this.marginHeight;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getMarginWidth() /*-{
    return this.marginWidth;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getName() /*-{
    return this.name;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getScrolling() /*-{
    return this.scrolling;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getSrc() /*-{
    return this.src;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isNoResize() /*-{
    return !!this.noResize;
    }-*/;

    /**
     *
     * @param frameBorder
     */
    public final native void setFrameBorder(int frameBorder) /*-{
    this.frameBorder = frameBorder;
    }-*/;

    /**
     *
     * @param longDesc
     */
    public final native void setLongDesc(String longDesc) /*-{
    this.longDesc = longDesc;
    }-*/;

    /**
     *
     * @param marginHeight
     */
    public final native void setMarginHeight(int marginHeight) /*-{
    this.marginHeight = marginHeight;
    }-*/;

    /**
     *
     * @param marginWidth
     */
    public final native void setMarginWidth(int marginWidth) /*-{
    this.marginWidth = marginWidth;
    }-*/;

    /**
     *
     * @param name
     */
    public final native void setName(String name) /*-{
    this.name = name;
    }-*/;

    /**
     *
     * @param noResize
     */
    public final native void setNoResize(boolean noResize) /*-{
    this.noResize = noResize;
    }-*/;

    /**
     *
     * @param scrolling
     */
    public final native void setScrolling(String scrolling) /*-{
    this.scrolling = scrolling;
    }-*/;

    /**
     *
     * @param src
     */
    public final native void setSrc(String src) /*-{
    this.src = src;
    }-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class AreaElement extends Element {

    /**
     *
     */
    public static final String TAG = "area";

    /**
     *
     */
    protected AreaElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static AreaElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(AreaElement.TAG)){
    		throw new ClassCastException("Can not covert to AreaElement");
    	}
    	return (AreaElement)elm;
    }

    /**
     *
     * @return
     */
    public final native String getAccessKey() /*-{
    return this.accessKey;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getAlt() /*-{
    return this.alt;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getCoords() /*-{
    return this.coords;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getHref() /*-{
    return this.href;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getShape() /*-{
    return this.shape;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getTarget() /*-{
    return this.target;
    }-*/;

    /**
     *
     * @param accessKey
     */
    public final native void setAccessKey(String accessKey) /*-{
    this.accessKey = accessKey;
    }-*/;

    /**
     *
     * @param alt
     */
    public final native void setAlt(String alt) /*-{
    this.alt = alt;
    }-*/;

    /**
     *
     * @param coords
     */
    public final native void setCoords(String coords) /*-{
    this.coords = coords;
    }-*/;

    /**
     *
     * @param href
     */
    public final native void setHref(String href) /*-{
    this.href = href;
    }-*/;

    /**
     *
     * @param shape
     */
    public final native void setShape(String shape) /*-{
    this.shape = shape;
    }-*/;

    /**
     *
     * @param target
     */
    public final native void setTarget(String target) /*-{
    this.target = target;
    }-*/;
}

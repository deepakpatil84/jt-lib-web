/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;

@NativeNonInstanciable
public class Text extends Node {

    /**
     *
     */
    protected Text() {
	}

    /**
     *
     * @param offset
     * @param length
     */
    @NoJavaScript
    public final native void deleteData(int offset, int length);
	
    /**
     *
     * @return
     */
    public final native String getData() /*-{
		return this.data;
	}-*/;

    /**
     *
     * @return
     */
    public final native int getLength() /*-{
		return this.length;
	}-*/;         

    /**
     *
     * @param offset
     * @param data
     */
    @NoJavaScript
    public final native void insertData(int offset, String data);
	
    /**
     *
     * @param offset
     * @param length
     * @param data
     */
    @NoJavaScript
    public final native void replaceData(int offset, int length, String data);

    /**
     *
     * @param data
     */
    public final native void setData(String data) /*-{
		this.data = data;
	}-*/;

	/**
	 * 
	 * @param offset
	 * @return
	 */
	public final native Text splitText(int offset) /*-{
		return this.splitText(offset);
	}-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.dom;
import com.jstype.fx.NativeNonInstanciable;

@NativeNonInstanciable
public class OptionElement extends Element {

    public static final String TAG = "option";

    /**
     *
     */
    protected OptionElement() {
    }
    public static boolean is(Object elm) {
		return Element.is(elm) ? ((Element) elm).getTagName().equalsIgnoreCase(TAG)
		        : false;
	}
    public static OptionElement as(Object elm){
    	
    	if(!((Element)elm).getTagName().equalsIgnoreCase(OptionElement.TAG)){
    		throw new ClassCastException("Can not covert to OptionElement");
    	}
    	return (OptionElement)elm;
    }
    /**
     *
     * @return
     */
    public final native FormElement getForm() /*-{
    return this.form;
    }-*/;

    /**
     *
     * @return
     */
    public final native int getIndex() /*-{
    return this.index;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getLabel() /*-{
    return this.label;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getText() /*-{
    return this.text;
    }-*/;

    /**
     *
     * @return
     */
    public final native String getValue() /*-{
    return this.value;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDefaultSelected() /*-{
    return !!this.defaultSelected;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isDisabled() /*-{
    return !!this.disabled;
    }-*/;

    /**
     *
     * @return
     */
    public final native boolean isSelected() /*-{
    return !!this.selected;
    }-*/;

    /**
     *
     * @param selected
     */
    public final native void setDefaultSelected(boolean selected) /*-{
    this.defaultSelected = selected;
    }-*/;

    /**
     *
     * @param disabled
     */
    public final native void setDisabled(boolean disabled) /*-{
    return this.disabled = disabled;
    }-*/;

    /**
     *
     * @param label
     */
    public final native void setLabel(String label) /*-{
    return this.label = label;
    }-*/;

    /**
     *
     * @param selected
     */
    public final native void setSelected(boolean selected) /*-{
    this.selected = selected;
    }-*/;

    /**
     *
     * @param text
     */
    //for IE6 innerHTML
    public final native void setText(String text) /*-{
    this.innerHTML = text;
    
    }-*/;

    /**
     *
     * @param value
     */
    public final native void setValue(String value) /*-{
    this.value = value;
    }-*/;
}

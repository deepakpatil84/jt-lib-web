/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.xhr;

public class XMLHttpRequest {

	public static final int UNSENT = 0;

	public static final int OPENED = 1;

	public static final int HEADERS_RECEIVED = 2;

	public static final int LOADING = 3;

	public static final int DONE = 4;

	private Object no;// native object

	private static native Object createNativeObject()
	/*-{
	  if (XMLHttpRequest) {
	    return new XMLHttpRequest();
	  } else {
	    try {
	      return new ActiveXObject('MSXML2.XMLHTTP.3.0');
	    } catch (e) {
	      return new ActiveXObject("Microsoft.XMLHTTP");
	    }
	  }
	}-*/;

	public XMLHttpRequest() {
		this.no = createNativeObject();
	}

	public final native void abort()
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.abort();
	}-*/;

	public final native void clearOnReadyStateChange()
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.onreadystatechange = function(){};
	}-*/;

	public final native String getAllResponseHeaders()
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.getAllResponseHeaders();
	}-*/;

	public final native int getReadyState()
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.readyState;
	}-*/;

	public final native String getResponseHeader(String header)
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.getResponseHeader(header);
	}-*/;

	public final native String getResponseText()
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.responseText;
	}-*/;

	public final native int getStatus()
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.status;
	}-*/;

	public final native String getStatusText()
	/*-{
	  return this.@com.jstype.web.client.xhr.XMLHttpRequest::no.statusText;
	}-*/;

	public final native void open(String httpMethod, String url)
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.open(httpMethod, url, true);
	}-*/;

	public final native void open(String httpMethod, String url, String user)
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.open(httpMethod, url, true, user);
	}-*/;

	public final native void open(String httpMethod, String url, String user,
	        String password)
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.open(httpMethod, url, true, user, password);
	}-*/;

	public final void send() {
		send(null);
	}

	public final native void send(String requestData)
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.send(requestData);
	}-*/;

	public final native void setOnReadyStateChange(
	        ReadyStateChangeHandler handler)
	/*-{
	  var o=this;
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.onreadystatechange = function() {
	    handler.@com.jstype.web.client.xhr.ReadyStateChangeHandler::onReadyStateChange(Lcom/jstype/web/client/xhr/XMLHttpRequest;)(o);
	  };
	}-*/;

	public final native void setRequestHeader(String header, String value)
	/*-{
	  this.@com.jstype.web.client.xhr.XMLHttpRequest::no.setRequestHeader(header, value);
	}-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client.xhr;

public class XMLHttpRequestStreaming {

    private XMLHttpRequest xhr;
    private int datacount = 0;

    public XMLHttpRequestStreaming(String method, String url) {
        xhr = new XMLHttpRequest();
        xhr.open(method, url);
    }

    public void send(StreamingDataHandler handler) {
        send(null, handler);
    }

    public void send(String requestData, final StreamingDataHandler handler) {
        xhr.setOnReadyStateChange(new ReadyStateChangeHandler() {

            public void onReadyStateChange(XMLHttpRequest xhr) {
                if (xhr.getReadyState() == XMLHttpRequest.LOADING) {
                    String data = xhr.getResponseText().substring(datacount);
                    datacount += data.length();
                    handler.onDataReceived(data);
                } else if (xhr.getReadyState() == XMLHttpRequest.DONE) {
                    handler.onComplete(xhr);
                }
            }
        });
        xhr.send(requestData);
    }

    public final void abort() {
        xhr.abort();
    }

    public final String getAllResponseHeaders() {
        return xhr.getAllResponseHeaders();
    }

    public final int getReadyState() {
        return xhr.getReadyState();
    }

    public final String getResponseHeader(String header) {
        return xhr.getResponseHeader(header);
    }

    public final String getResponseText() {
        return xhr.getResponseText();
    }

    public final int getStatus() {
        return xhr.getStatus();
    }

    public final String getStatusText() {
        return xhr.getStatusText();
    }

    public final void setRequestHeader(String header, String value) {
        xhr.setRequestHeader(header, value);
    }
}

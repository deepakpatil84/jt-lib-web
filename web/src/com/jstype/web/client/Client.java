/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.jstype.web.client;

/**
 * @author Deepak Patil
 *
 */

/*
 * TODO:We can write in compact by combining function like checking presence of string in UserAgent
 */
public final class Client {

    public static boolean SAFARI;
    public static boolean SAFARI_3;
    public static boolean SAFARI_3_OR_ABOVE;
    public static boolean SAFARI_4;
    public static boolean SAFARI_4_OR_ABOVE;
    public static boolean SAFARI_5;
    public static boolean SAFARI_5_OR_ABOVE;
    public static boolean IE;
    public static boolean IE_6;
    public static boolean IE_6_OR_ABOVE;
    public static boolean IE_7;
    public static boolean IE_7_OR_ABOVE;
    public static boolean IE_8;
    public static boolean IE_8_OR_ABOVE;
    public static boolean IE_9;
    public static boolean IE_9_OR_ABOVE;

    public static boolean FIREFOX;
    public static boolean FIREFOX_2;
    public static boolean FIREFOX_2_OR_ABOVE;
    public static boolean FIREFOX_3;
    public static boolean FIREFOX_3_OR_ABOVE;
    public static boolean FIREFOX_4;
    public static boolean FIREFOX_4_OR_ABOVE;
    public static boolean FIREFOX_5;
    public static boolean FIREFOX_5_OR_ABOVE;
    public static boolean FIREFOX_6;
    public static boolean FIREFOX_6_OR_ABOVE;
    public static boolean FIREFOX_7;
    public static boolean FIREFOX_7_OR_ABOVE;
    public static boolean FIREFOX_8; //futur releases as of 9/29/2011
    public static boolean FIREFOX_8_OR_ABOVE;
    public static boolean FIREFOX_9;
    public static boolean FIREFOX_9_OR_ABOVE;

    public static boolean CHROME;
    public static boolean OPERA;
    public static boolean OMNIWEB;
    public static boolean KONQUEROR;
    public static boolean ICAB;
    public static boolean CAMINO;
    public static boolean NETSCAPE;
    public static boolean MAXTHON;
    public static boolean SEAMONKEY;
    public static boolean UNKNOWN_BROWSER = true;
    //Operating System -Desktop
    public static boolean OS_LINUX;
    public static boolean OS_UNIX;
    public static boolean OS_BSD;
    public static boolean OS_WINDOWS;
    public static boolean OS_MAC;
    //Operating System -Mobile/Handheld
    public static boolean OS_IOS;
    public static boolean OS_ANDROID;
    public static boolean OS_SYMBIAN;
    public static boolean OS_BLACKBERYY;
    public static boolean OS_WIN_CE;
    //
    public static boolean OS_UNKNOWN = true;
    //Types of device
    public static boolean DEVICE_DESKTOP;
    public static boolean DEVICE_HANDHELD;
    public static boolean DEVICE_MOBILE;
    public static boolean DEVICE_UNKNOWN = true;
    //Layout engines
    public static boolean LE_GECKO;
    public static boolean LE_TRIDENT;
    public static boolean LE_WEBKIT;
    public static boolean LE_PRESTO;
    public static boolean LE_KHTML;
    public static boolean LE_UNKNOWN;

    /**
     * No need to run this method, will be automatically called when first loaded
     * @skip
     */
    public static void init() {

        UNKNOWN_BROWSER = false;
        if (isIE()) {
            IE = true;
            float v=getIEVersion();
            if(v>=9){
                IE_9=true;
                IE_9_OR_ABOVE=true;
                IE_8_OR_ABOVE=true;
                IE_7_OR_ABOVE=true;
                IE_6_OR_ABOVE=true;
            }else if(v>=8){
                IE_8=true;
                IE_8_OR_ABOVE=true;
                IE_7_OR_ABOVE=true;
                IE_6_OR_ABOVE=true;
            }else if(v>=7){
                IE_7=true;
                IE_7_OR_ABOVE=true;
                IE_6_OR_ABOVE=true;
            }else if(v>=6){
                IE_6=true;
                IE_6_OR_ABOVE=true;
            }//else dont know which version is this


        } else if (isFirefox()) {
            FIREFOX = true;
            float v=getFirefoxVersion();
            if(v>=9){
                FIREFOX_9=true;
                FIREFOX_9_OR_ABOVE=true;
                FIREFOX_8_OR_ABOVE=true;
                FIREFOX_7_OR_ABOVE=true;
                FIREFOX_6_OR_ABOVE=true;
                FIREFOX_5_OR_ABOVE=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=8){
                FIREFOX_8=true;
                FIREFOX_8_OR_ABOVE=true;
                FIREFOX_7_OR_ABOVE=true;
                FIREFOX_6_OR_ABOVE=true;
                FIREFOX_5_OR_ABOVE=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=7){
                FIREFOX_7=true;
                FIREFOX_7_OR_ABOVE=true;
                FIREFOX_6_OR_ABOVE=true;
                FIREFOX_5_OR_ABOVE=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=6){
                FIREFOX_6=true;
                FIREFOX_6_OR_ABOVE=true;
                FIREFOX_5_OR_ABOVE=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=5){
                FIREFOX_5=true;
                FIREFOX_5_OR_ABOVE=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=4){
                FIREFOX_4=true;
                FIREFOX_4_OR_ABOVE=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=3){
                FIREFOX_3=true;
                FIREFOX_3_OR_ABOVE=true;
                FIREFOX_2_OR_ABOVE=true;
            }else if(v>=2){
                FIREFOX_2=true;
                FIREFOX_2_OR_ABOVE=true;
            }
        } else if (isOpera()) {
            OPERA = true;

        } else if (isChrome()) {
            CHROME = true;

        } else {
            UNKNOWN_BROWSER = true;
        }



        LE_UNKNOWN = false;
        if (isGecko()) {
            LE_GECKO = true;

        } else if (isWebKit()) {
            LE_WEBKIT = true;

        } else if (isPresto()) {
            LE_PRESTO = true;

        } else if (isTrident()) {
            LE_TRIDENT = true;

        } else {
            LE_UNKNOWN = true;
        }


    }

    public static native String getUserAgent()
    /*-{
        return navigator.userAgent||'';
    }-*/;
    public static native boolean isGecko()
    /*-{
    	try{
    		return navigator.userAgent.indexOf("Gecko")!=-1 && navigator.userAgent.indexOf("Safari")==-1;
    	}catch(e){}
    	return false;
    }-*/;

    public static native boolean isTrident()
    /*-{
    	try{
    		return navigator.userAgent.indexOf("Trident")!=-1;
    	}catch(e){}
    	return false;
    }-*/;

    public static native boolean isWebKit()
    /*-{
    	try{
    		return navigator.userAgent.indexOf("WebKit")!=-1;
    	}catch(e){}
    	return false;
    }-*/;

    public static native boolean isPresto()
    /*-{
    	try{
    		return navigator.userAgent.indexOf("Presto")!=-1;
    	}catch(e){}
    	return false;
    }-*/;

    //DETECT OS
    public static native boolean isWindows()/*-{
    try{
    return navigator.platform.indexOf("Win") !=-1;
    }catch(e){}
    return false;
    }-*/;

    public static native boolean isMac()/*-{
    try{
    return navigator.platform.indexOf("Mac") !=-1;
    }catch(e){}
    return false;
    }-*/;

    public static native boolean isLinux()/*-{
    try{
    return navigator.platform.indexOf("Linux") !=-1;
    }catch(e){}
    return false;
    }-*/;

    public static native boolean isIOS()/*-{
    try{
    return (navigator.userAgent.indexOf("iPhone") !=-1) || (navigator.userAgent.indexOf("iPod") !=-1);
    }catch(e){}
    return false;
    }-*/;

    /**
     * Check if current client is Android
     * <code>true</code> if Android else <code>false</code>
     * @return boolean
     */
    public static native boolean isAndroid()/*-{
    try{
    return navigator.platform.indexOf("Android") !=-1;
    }catch(e){}
    return false;
    }-*/;

    /**
     * Check if current client is Symbian
     * <code>true</code> if Symbian else <code>false</code>
     * @return boolean
     */
    public static native boolean isSymbian() /*-{
		try{
			return navigator.userAgent.toLowerCase().indexOf("symbian")!=-1;
		}catch(e){}
		return false;
	}-*/;

    /**
     * Check if current client is Blackberry
     * <code>true</code> if Blackberry else <code>false</code>
     * @return boolean
     */
    public static native boolean isBlackberry() /*-{
    	try{
			return navigator.userAgent.toLowerCase().indexOf("blackberry")!=-1;
		}catch(e){}
		return false;
	}-*/;

    /**
     * Check if current client is WinCE(Windows Compact Edition)
     * <code>true</code> if WinCE else <code>false</code>
     * @return boolean
     */
    public static boolean isWinCE() {
        return false;
    }

    /**
     * Check if current client is Opera
     * <code>true</code> if Opera else <code>false</code>
     * @return boolean
     */
    public static native boolean isOpera()/*-{
    return window.opera!=undefined;
    }-*/;

    /**
     * Check if current client is Safari
     * <code>true</code> if Safari else <code>false</code>
     * @return boolean
     */
    public static native boolean isSafari()/*-{
    try{
    return navigator.vendor.indexOf("Apple")!=-1;
    }catch(e){}
    return false;
    }-*/;
    
    /**
     * Check if current client is Firefox
     * <code>true</code> if Firefox else <code>false</code>
     * @return boolean
     */
    
    public static native boolean isFirefox()/*-{
    try{
    	return navigator.userAgent.indexOf("Firefox")!=-1;
    }catch(e){}
    	return false;
    }-*/;
    
    /**
     * Get the current version of Mozilla Firefox
     * return -1 in case of non Internet Explorer or other clients
     * @return float version of  Firefox
     */
    
    public static native float getFirefoxVersion()/*-{
        if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
            return parseInt(RegExp.$1)
        }
     return -1;
     }-*/;
    
    /**
     * Check if current client is Chrome
     * <code>true</code> if Chrome else <code>false</code>
     * @return boolean
     */
    public static native boolean isChrome()/*-{
    try{
    return navigator.userAgent.indexOf("Chrome")!=-1;
    }catch(e){}
    return false;
    }-*/;
    
    /**
     * Check if current client is Internet Explorer
     * <code>true</code> if IE else <code>false</code>
     * @return boolean
     */
    public static native boolean isIE()/*-{
    	try{
    		return navigator.appName == "Microsoft Internet Explorer";
    	}catch(e){}
    	return false;
    }-*/;

    /**
     * Get the current version of Microsoft Internet Explorer
     * return -1 in case of non Internet Explorer or other clients
     * @return float version of IE
     * @ref:http://support.microsoft.com/kb/167820
     */
    public static native float getIEVersion()/*-{
    var ua = window.navigator.userAgent,
		msie = ua.indexOf ( "MSIE " )
    if (msie>0)
    	return parseFloat(ua.substring (msie+5, ua.indexOf (".", msie )))
    return -1;
    }-*/;
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client;

import java.util.HashMap;

public class Location {
	private Object obj;
	private HashMap<String, String> pars;

	public Location() {
	}

	private void createMap() {
		if (pars == null) {
			pars = new HashMap<String, String>();
			String queryString = getQueryString();
			if (queryString != null && queryString.length() > 1) {
				String qs = queryString.substring(1);
				for (String kvPair : qs.split("&")) {
					String[] kv = kvPair.split("=", 2);
					if (kv.length > 1) {
						pars.put(kv[0], decodeQueryString(kv[1]));
					} else {
						pars.put(kv[0], "");
					}
				}
			}
		}
	}

	public String getParamter(String name) {
		createMap();
		return pars.get(name);
	}

	private static native String decodeQueryString(String s)
	/*-{
	    return decodeURIComponent(s.replace(/\+/g, "%20"));
	}-*/;

	public native String getQueryString()
	/*-{
		var h=window.location.href,
			i=h.indexOf("#"),
			q;
	    if(i>=0)
			h=h.substring(0,i);        
	    q=h.indexOf("?");
	    return ( q > 0 )?h.substring(q):"";
	}-*/;

	public native String getQueryStringStd()
	/*-{
	    return window.location.search;
	}-*/;

	private native String getQueryStringIE()
	/*-{
	  var h=window.location.href,
	  	  L=h.indexOf("#"),
	  	  q;
	  if(L>=0)
	  	h=h.substring(0,L);        
	  q=h.indexOf("?");
	  return (q>0) ? h.substring(q) :"";
	}-*/;

	public void setObject(Object obj) {
		this.obj = obj;
	}

	public Object getObject() {
		return this.obj;
	}

	public native String getHash()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.hash;
	}-*/;

	public native String getHost()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.host;
	}-*/;

	public native String getHostname()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.hostname;
	}-*/;

	public native String getHref()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.href;
	}-*/;

	public native String getPathname()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.pathname;
	}-*/;

	public native String getPort()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.port;
	}-*/;

	/**
	 * 
	 * @return
	 */
	public native String getProtocol()
	/*-{
		return this.@com.jstype.web.client.Location::obj.protocol;
	}-*/;

	/**
	 * 
	 * @return
	 */
	public native String getSearch()
	/*-{
	    return this.@com.jstype.web.client.Location::obj.search;
	}-*/;

	/**
	 * 
	 * @param url
	 */
	public native void assign(String url)
	/*-{
	    this.@com.jstype.web.client.Location::obj.assign(url);
	}-*/;

	/**
     *
     */
	public native void reload()
	/*-{
	    this.@com.jstype.web.client.Location::obj.reload();
	}-*/;

	/**
	 * 
	 * @param url
	 */
	public native void replace(String url)
	/*-{
	    this.@com.jstype.web.client.Location::obj.replace(url);
	}-*/;

}

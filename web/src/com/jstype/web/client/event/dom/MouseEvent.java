/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.event.dom;

import com.jstype.web.client.event.EventHandler;

public abstract class MouseEvent<H extends EventHandler>  extends DomEvent<H>{
        public int getClientX(){
            return getNativeEvent().getClientX();
        }
        
        public int getClientY(){
            return getNativeEvent().getClientY();
        }

        public int getNativeButton(){
            return getNativeEvent().getButton();
        }

        public int getScreenX(){
            return getNativeEvent().getScreenX();
        }
        public int getScreenY(){
            return getNativeEvent().getScreenY();
        }


}

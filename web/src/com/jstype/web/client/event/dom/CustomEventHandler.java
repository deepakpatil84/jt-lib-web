package com.jstype.web.client.event.dom;

import com.jstype.web.client.event.EventHandler;
import com.jstype.web.client.dom.NativeEvent;

public interface CustomEventHandler extends EventHandler{
	public void onEvent(NativeEvent event);
}

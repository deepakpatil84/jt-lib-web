/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.event.dom;

import com.jstype.web.client.event.EventHandler;

public abstract class KeyEvent<H extends EventHandler> extends DomEvent<H> {
    public boolean isAltKeyDown(){
        return getNativeEvent().getAltKey();
    }
    public boolean isCtrlKeyDown(){
        return getNativeEvent().getCtrlKey();
    }
    public boolean isShiftKeyDown(){
        return getNativeEvent().getShiftKey();
    }
    public boolean isMetaKeyDown(){
        return getNativeEvent().getMetaKey();
    }
    public boolean isAnyModifierKeyDown(){
        return isAltKeyDown() || isCtrlKeyDown() || isShiftKeyDown() || isMetaKeyDown();
    }
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.event;

/**
 * http://unixpapa.com/js/key.html
 */
public class KeyCodes {

    private KeyCodes() {
    }
    public static final int KEY_SPACE = 32;
    public static final int KEY_ENTER = 13;
    public static final int KEY_TAB = 9;
    public static final int KEY_ESCAPE = 27;
    public static final int KEY_BACKSPACE = 8;
    public static final int KEY_SHIFT = 16;
    public static final int KEY_CTRL = 17;
    public static final int KEY_ALT = 18;
    public static final int KEY_LEFT = 37;
    public static final int KEY_UP = 38;
    public static final int KEY_RIGHT = 39;
    public static final int KEY_DOWN = 40;
    public static final int KEY_INSERT = 45;
    public static final int KEY_DELETE = 46;
    public static final int KEY_HOME = 36;
    public static final int KEY_END = 35;
    public static final int KEY_PAGEUP = 33;
    public static final int KEY_PAGEDOWN = 34;
}

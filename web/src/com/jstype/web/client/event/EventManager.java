/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.event;

import java.util.ArrayList;

import com.jstype.web.client.event.dom.CustomEventHandler;
import com.jstype.web.client.dom.NativeEvent;

import java.util.HashMap;

public class EventManager {

	private HashMap<String, ArrayList<EventHandler>> handlers;

	public EventManager() {

		handlers = new HashMap<String, ArrayList<EventHandler>>();
	}

	public void addEventHandler(String type, EventHandler handler) {
		doAddEventHandler(type, handler);
	}

	private void doAddEventHandler(String name, final EventHandler handler) {
		if (!handlers.containsKey(name)) {
			handlers.put(name, new ArrayList<EventHandler>());
		}
		handlers.get(name).add(handler);

	}

	public void removeEventHandler(String type, EventHandler handler) {
		String name = type;
		if (handlers.containsKey(name)) {
			ArrayList<EventHandler> handlersList = handlers.get(name);
			if (handlersList.contains(handler)) {
				handlersList.remove(handler);
			}
			if (handlersList.isEmpty()) {
				handlers.remove(name);
			}
		}
	}

	public boolean hasEventHandlersForType(String type) {
		return handlers.containsKey(type);
	}

	public void fireCustomEvent(NativeEvent event) {
		ArrayList<EventHandler> handlersList = handlers.get(event.getType());
		if (handlersList != null) {

			CustomEventHandler[] harray = new CustomEventHandler[handlersList
			        .size()];
			harray = handlersList.<CustomEventHandler> toArray(harray);
			for (CustomEventHandler handler : harray) {
				try {
					handler.onEvent(event);
				} catch (Exception e) {

				}
			}
		}
	}

	public void fireEvent(String type, Event event) {
		ArrayList<EventHandler> handlersList = handlers.get(type);
		if (handlersList != null) {
			EventHandler[] harray = new EventHandler[handlersList.size()];
			harray = handlersList.<EventHandler> toArray(harray);
			for (EventHandler handler : harray) {
				try {
					event.dispatch(handler);
				} catch (Exception e) {
				}
			}
		}
	}

}

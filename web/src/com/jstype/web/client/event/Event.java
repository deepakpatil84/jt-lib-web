/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.client.event;

public abstract  class Event<H> {

    public static final int BLUR        = 1;
    public static final int CHANGE      = 2;
    public static final int CLICK       = 3;
    public static final int CONTEXTMENU = 4;
    public static final int DBLCLICK     = 5;
    public static final int FOCUS       = 6;
    public static final int KEYDOWN     = 7;
    public static final int KEYPRESS    = 8;
    public static final int KEYUP       = 9;
    public static final int LOAD        = 10;
    public static final int MOUSEDOWN   = 11;
    public static final int MOUSEMOVE   = 12;
    public static final int MOUSEOUT    = 13;
    public static final int MOUSEOVER   = 14;
    public static final int MOUSEUP     = 15;
    

    protected Event() {
    }

    protected abstract void dispatch(H handler);
    
    
    public static int toEventTypeInt(String eventType){
        if(eventType.equals("blur")) return Event.BLUR;
        if(eventType.equals("change")) return Event.CHANGE;
        if(eventType.equals("click")) return Event.CLICK;
        if(eventType.equals("contextmenu")) return Event.CONTEXTMENU;
        if(eventType.equals("dblclick")) return Event.DBLCLICK;
        if(eventType.equals("focus")) return Event.FOCUS;
        if(eventType.equals("keydown")) return Event.KEYDOWN;
        if(eventType.equals("keypress")) return Event.KEYPRESS;
        if(eventType.equals("keyup")) return Event.KEYUP;
        if(eventType.equals("load")) return Event.LOAD;
        if(eventType.equals("mousedown")) return Event.MOUSEDOWN;
        if(eventType.equals("mousemove")) return Event.MOUSEMOVE;
        if(eventType.equals("mouseout")) return Event.MOUSEOUT;
        if(eventType.equals("mouseover")) return Event.MOUSEOVER;
        if(eventType.equals("mouseup")) return Event.MOUSEUP;
        return -1;
   }

}

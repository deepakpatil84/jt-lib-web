/**
 * 
 */
package com.jstype.web.storage;

/**
 * @author Deepak Patil
 *
 */
public class SessionStorage  {

	/* (non-Javadoc)
	 * @see com.ojstypeweb.storage.Storage#getLength()
	 */

	public static native int getLength() 
	/*-{
	 	return sessionStorage.length;
	}-*/;
	 

	/* (non-Javadoc)
	 * @see com.opjstypeeb.storage.Storage#key(int)
	 */
	
	public static native String key(int index) 
	/*-{
	 	return sessionStorage.key(index);
	 }-*/;

	/* (non-Javadoc)
	 * @see com.opejstypeb.storage.Storage#getItem(java.lang.String)
	 */
	
	public static native String getItem(String key) 
	/*-{
	 	return sessionStorage.getItem(key);
	 }-*/;

	/* (non-Javadoc)
	 * @see com.openjstype.storage.Storage#setItem(java.lang.String, java.lang.String)
	 */
	public static native void setItem(String key, String value)  
	/*-{
 		return sessionStorage.setItem(key,value);
 	}-*/;

	/* (non-Javadoc)
	 * @see com.openwjstypestorage.Storage#deleteItem(java.lang.String)
	 */
	public static native void deleteItem(String key) 
	/*-{
 		return sessionStorage.deleteItem(key);
 	}-*/;

	/* (non-Javadoc)
	 * @see com.openwaf.web.storage.Storage#clear()
	 */
	public static native void clear()  
	/*-{
 		return sessionStorage.clear();
 	}-*/;

}

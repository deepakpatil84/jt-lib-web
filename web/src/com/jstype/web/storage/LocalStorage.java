/**
 * 
 */
package com.jstype.web.storage;

import org.json.JSONObject;

/**
 * @author Deepak Patil
 * 
 */
public class LocalStorage {
	
	private LocalStorage(){
		
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.ojstypeweb.storage.Storage#getLength()
	 */
	public static native int getLength()
	/*-{
	 	return localStorage.length;
	}-*/;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opjstypeeb.storage.Storage#key(int)
	 */
	public static native String key(int index)
	/*-{
	 	return localStorage.key(index);
	 }-*/;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.opejstypeb.storage.Storage#getItem(java.lang.String)
	 */
	public static native String getItem(String key)
	/*-{
	 	return localStorage.getItem(key);
	 }-*/;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.openjstype.storage.Storage#setItem(java.lang.String,
	 * java.lang.String)
	 */
	public static native void setItem(String key, String value)
	/*-{
		return localStorage.setItem(key,value);
	}-*/;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.openwjstypestorage.Storage#removeItem(java.lang.String)
	 */
	public static native void removeItem(String key)
	/*-{
		return localStorage.removeItem(key);
	}-*/;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.openwaf.web.storage.Storage#clear()
	 */
	public static native void clear()
	/*-{
		return localStorage.clear();
	}-*/;

	public static native JSONObject getJSON(String key)
	/*-{
		return window.localStorage[key] ? JSON.parse(window.localStorage[key]) : null;
	}-*/;

	public static native void set(String key, Object object)
	/*-{
		window.localStorage.setItem(key,JSON.stringify(object));
	}-*/;

}

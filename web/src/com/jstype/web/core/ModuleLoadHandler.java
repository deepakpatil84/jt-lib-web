package com.jstype.web.core;
/**
 * Async handler to handle onload/onerror event after loading module
 * @author Deepak Patil
 *
 */
public abstract class ModuleLoadHandler {
    /**
     * If module is already loaded this get called immediately if not then this get called back 
     * after loading module through AJAX
     */
    public abstract void onLoad();
    /**
     *  No action by default you must override this to handle error in case of module is not loaded
     * @param errorText
     */
    public void onError(String errorText){
    }
}

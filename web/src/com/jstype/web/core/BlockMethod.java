/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.core;

import com.jstype.fx.EnsureIdentifier;

public class BlockMethod {
	/*
	 * Count of number of
	 */

	@EnsureIdentifier
	private static int SB = 0;
	@EnsureIdentifier
	private static int SR = 1;
	@EnsureIdentifier
	private static int SN = 2;

	@EnsureIdentifier
	private static int cc = 0;

	@EnsureIdentifier
	private static Object g;

	// ov_getGCCount
	public BlockMethod() {
		this.init();
	}

	@EnsureIdentifier
	public final native void init()
	/*-{
	   this.g={};
	}-*/;

	@EnsureIdentifier
	public int n() {
		return cc++;
	}

	// getState
	@EnsureIdentifier
	public native int s(int g)
	/*-{
	    if(this.g[g]==undefined){ return -1; }        
	    return this.g[g].s;
	}-*/;

	// setState
	@EnsureIdentifier
	public native void t(int g, int s)
	/*-{
	    this.g[g].s=s;
	}-*/;

	// initBFCall
	/*
	 * r .. return values
	 * cb call back
	 * co calling object
	 * ac argument count
	 * ex exception
	 * x xmlhttp object
	 * s current state
	 * m method stack
	 */
	@EnsureIdentifier
	public native void i(int g)
	/*-{
	    var k={},m=[],n={};
	    this.g[g]=k;
	    k.r=k.cb=k.co=k.ac=k.ex=k.x=null;
	    k.m=m;
	    m[0]=n;
	    n.v=[]
	    n.r=-1;
	    k.s=this.SN;
	}-*/;

	// initBFCallFun
	@EnsureIdentifier
	public native void f(int g, int m)
	/*-{
	    var k={};
	    this.g[g].m[m]=k;
	    k.v=[]
	    k.r=-1;
	    this.g[g].s=this.SN;
	}-*/;

	// setRC
	@EnsureIdentifier
	public native void l(int g, int m, int c)
	/*-{
	    this.g[g].m[m].r=c;
	}-*/;

	// getRC
	@EnsureIdentifier
	public native int k(int g, int m)
	/*-{
	    return this.g[g].m[m].r;
	}-*/;

	// setVar
	@EnsureIdentifier
	public native void m(int g, int m, String n, Object v)
	/*-{
	    this.g[g].m[m].v[n]=v;
	}-*/;

	// getVar
	@EnsureIdentifier
	public native int p(int g, int m, String n)
	/*-{
	    return this.g[g].m[m].v[n];
	}-*/;

	// getXMLHTTPObject
	@EnsureIdentifier
	public native Object o()
	/*-{
		if (window.XMLHttpRequest){
			return new XMLHttpRequest();
		}
		if (window.ActiveXObject) {
			return new ActiveXObject("Microsoft.XMLHTTP");
		}
		return null;
	}-*/;

	// TODO:implement this in JSON
	// getXMLDoc
	@EnsureIdentifier
	public native Object x(String s)
	/*-{
		if (window.DOMParser){			
			return (new DOMParser()).parseFromString(s,"text/xml");
		}
		x=new ActiveXObject("Microsoft.XMLDOM");
		x.async="false";
		x.loadXML(s);
	    return x;
	}-*/;

	@EnsureIdentifier
	private native Object xn(Object o)
	/*- {
		var n,i,r=[];
		for(i=0;i<o.childNodes.length;i++){
			n=o.childNodes[i];
			if(n.nodeType==1){r.push(n);}
		}
		return r;
	} -*/;

	@EnsureIdentifier
	public native String s_s(String[] o)
	/*- {
	    try{
		var i=0,s="";
		for(i=0;i<o.length;i++){
			if(i>0) s+=",";
			s+=("\"" + encodeURI(o[i]) + "\"");
		}
		return "[" + s + "]";
	    }
	    catch(e){}
	    return "";

	} -*/;

	@EnsureIdentifier
	public native String s_n(int[] o)
	/*- {
	    try{
		return "[" + o.join(",") +"]";
	    }
	    catch(e){}
	    return "";

	} -*/;
}

package com.jstype.web.core;

public  interface Module {
    
    void onModuleLoad();
    
}

/*
 * Copyright 2011 JsType.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.jstype.web.core;

import com.jstype.web.server.JSONSerializable;

public class JSONHelper {
	public static String stringToJSON(String s) {
		if (s == null) {
			return "null";
		}
		return '"' + encodeString(s) + '"';
	}

	public native static String encodeString(String s)
	/*-{
		if(s!=null || s!=undefined){
			if(typeof s == "string"){
				return s.replace("\\\"","\\\\\"");
			}
		}
		return s;
	}-*/;

	public native static String stringArrayToJSONString(String[] s)
	/*-{
	    if (s == null) return "null";
	    var i=0,jsa=[],l=s.length;
	    for(i=0;i<l;i++){
	        jsa.push('"'+@com.jstype.web.core.JSONHelper::encodeString(Ljava/lang/String;)(s[i])+'"');
	    }
	    return '['+jsa.join(",")+']';
	}-*/;

	public native static String sharedTypeArrayToJSONString(JSONSerializable[] s)
	/*-{
	  if(s==null)return null;
	    var i,jsa=[],l=s.length;
	    for(i=0;i<l; i++) {
	        if(s[i]!=null)
	            jsa.push(s[i].@com.jstype.web.server.JSONSerializable::toJSON()());
	        else
	            jsa.push(null);
	    }
	    return '['+jsa.join(",")+']';
	}-*/;
}

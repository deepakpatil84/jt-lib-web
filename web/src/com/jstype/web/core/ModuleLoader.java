package com.jstype.web.core;

import java.util.ArrayList;
import java.util.HashMap;

import com.jstype.web.client.Window;
import com.jstype.web.client.dom.Document;
import com.jstype.web.client.dom.ScriptElement;
import com.jstype.web.client.event.dom.LoadEvent;
import com.jstype.web.client.event.dom.LoadHandler;

public final class ModuleLoader {
	private static HashMap<Class, Boolean> loadedmodules;
	private static HashMap<Class, ArrayList<ModuleLoadHandler>> onloadhandlers;
	private static Object mapping;

	static {

		onloadhandlers = new HashMap<Class, ArrayList<ModuleLoadHandler>>();
		loadedmodules = new HashMap<Class, Boolean>();
		init();
	}

	private static native void init()
	/*-{
		return @com.jstype.web.core.ModuleLoader::mapping = {};
	}-*/;

	public static <E extends Module> void loadModule(
	        final Class<E> moduleClass, ModuleLoadHandler loadhandler)
	        throws Exception {
		if (loadedmodules.containsKey(moduleClass)) {
			loadhandler.onLoad();
		} else {

			ArrayList<ModuleLoadHandler> list;

			if (onloadhandlers.containsKey(moduleClass)) {
				list = onloadhandlers.get(moduleClass);
			} else {
				list = new ArrayList<ModuleLoadHandler>();
				onloadhandlers.put(moduleClass, list);
			}

			list.add(loadhandler);
			String modulepath = getClassToURL(moduleClass.getName());

			Document d = Window.getDocument();
			ScriptElement scr = (ScriptElement) d
			        .createElement(ScriptElement.TAG);
			scr.setType("text/javascript");
			scr.setSrc(modulepath);
			scr.addLoadHandler(new LoadHandler() {

				public void onLoad(LoadEvent event) {
					loadedmodules.put(moduleClass, Boolean.valueOf(true));

					ArrayList<ModuleLoadHandler> list = onloadhandlers
					        .get(moduleClass);

					for (ModuleLoadHandler handler : list) {
						handler.onLoad();
					}
					list.clear();
					onloadhandlers.remove(moduleClass);

				}
			});
			d.getBodyElement().appendChild(scr);

		}
	}

	private static native String getClassToURL(String c)
	/*-{
		//return @com.jstype.web.core.ModuleLoader::mapping[c];
		return $ow_module_mapping[c] || null;
	}-*/;

	private static native void eval(String d)
	/*-{
		return eval('(' + d + ')');
	}-*/;

}
